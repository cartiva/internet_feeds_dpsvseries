program ArkonaInpmastNightly;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Classes,
  adsdata,
  adsfunc,
  adstable,
  adscnnct,
  DB,
  ADODB,
  DateUtils,
  IdSMTPBase,
  IdSMTP,
  IdMessage,
  Dialogs;

var
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;

begin
  try
    AdoCon := TADOConnection.Create(nil);
    AdsCon := TADSConnection.Create(nil);
    AdoQuery := TADOQuery.Create(nil);
    AdsQuery := TADSQuery.Create(nil);
//    AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=odbc0210;Persist Security Info=True;User ID=rydeodbc;Data Source=Arkona-SSL';
    AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
    AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DpsVSeries\DpsVSeries.add';
    AdsCon.Username := 'adssys';
    AdsCon.Password := 'cartiva';
    AdoCon.Connected := True;
    AdsCon.Connect;
    try
      try
        AdsQuery.AdsConnection := AdsCon;
        AdsQuery.SQL.Text := 'execute procedure sp_zaptable(''ArkonaInpmastNightly'')';
        AdsQuery.ExecSQL;
        AdsQuery.Close;
        AdsQuery.SQl.Text := 'insert into ArkonaInpmastNightly values ' +
            '(:stocknumber, :vin, :modelyear, :make, :model, :body,' +
            ':extcolor, :intcolor, :odometer, :msrp, :bestprice)';
        AdoQuery.Connection := AdoCon;
        AdoQuery.SQL.Text :=
          'select imstk#, imvin, imyear, immake, immodl, imbody, imcolr, imtrim, imodom, impric, f.ionval ' +
          'from rydedata.inpmast i ' +
          'inner join ( ' +
          '  select gtacct, gtctl#, sum(gttamt) as net ' +
          '  from rydedata.glptrns  ' +
          '  where  trim(gtpost) <> ''V'' ' +
          '  group by gtacct, gtctl# ' +
          '      having sum(coalesce(gttamt, 0)) > 1) ia  on trim(i.imstk#) = trim(ia.gtctl#) ' +
          'inner join ( ' +
          '  select distinct gmacct ' +
          '  from rydedata.glpmast ' +
          '  where ( ' +
          '    gmdesc like ''INV N/%'' ' +
          '    or ' +
          '    gmdesc like ''INV-NEW%'' ' +
          '    or ' +
          '    gmdesc like ''INV NEW%'')) a on trim(ia.gtacct) = trim(a.gmacct) ' +
          'left join rydedata.inpoptf f on f.iovin = i.imvin and f.ioseq# = ''9'' ' +
          'where imstat = ''I'' ' +
          '  and imtype = ''N''';

        AdoQuery.Open;
        while not AdoQuery.Eof do
        begin
          AdsQuery.ParamByName('stocknumber').AsString := AdoQuery.FieldByName('imstk#').AsString;
          AdsQuery.ParamByName('vin').AsString := AdoQuery.FieldByName('imvin').AsString;
          AdsQuery.ParamByName('modelyear').AsInteger := AdoQuery.FieldByName('imyear').AsInteger;
          AdsQuery.ParamByName('make').AsString := AdoQuery.FieldByName('immake').AsString;
          AdsQuery.ParamByName('model').AsString := AdoQuery.FieldByName('immodl').AsString;
          AdsQuery.ParamByName('body').AsString := AdoQuery.FieldByName('imbody').AsString;
          AdsQuery.ParamByName('extcolor').AsString := AdoQuery.FieldByName('imcolr').AsString;
          AdsQuery.ParamByName('intcolor').AsString := AdoQuery.FieldByName('imtrim').AsString;
          AdsQuery.ParamByName('odometer').AsString := AdoQuery.FieldByName('imodom').AsString;
          AdsQuery.ParamByName('msrp').AsCurrency := AdoQuery.FieldByName('impric').AsCurrency;
          AdsQuery.ParamByName('bestprice').AsCurrency := AdoQuery.FieldByName('ionval').AsCurrency;
          AdsQuery.ExecSQL;;
          AdoQuery.Next;
        end;
        AdsQuery.Close;
        AdsQuery.SQL.Clear;
        AdsQuery.SQL.Text := 'execute procedure sp_TrimCharacterFields(''ArkonaInpmastNightly'')';
        AdsQuery.ExecSQL;
        AdsQuery.Close;
        AdsQuery.SQL.Clear;
        AdsQuery.SQL.Text := 'UPDATE ArkonaInpmastNightly SET body = replace(body, ''"'', '''') WHERE body LIKE ''%"%''';
        AdsQuery.ExecSQL;
      except
        on E: Exception do
        begin
          Raise Exception.Create('ArkonaInpmastNightly.  MESSAGE: ' + E.Message);
        end;
      end;
    finally
      AdoQuery.Close;
      FreeAndNil(AdoQuery);
      AdsQuery.Close;
      FreeAndNil(AdsQuery);
      AdsCon.Disconnect;
      FreeAndNil(adsCon);
      AdoCon.Connected := False;
      FreeAndNil(AdoCon);
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
