{This program relies on two external files and an external Advantage table

  appname.ini (where appname is the name of this project)
    This file contains the valid markets, locatoin, the connection string, notification email addresses,
    ftp address and user name and directories where data will be written.

  logger.ini
    This file contains the connection information for a free Advantage table that will hold the log for this run

  External Advantage table
    This file contains the log for the run. Connection information is stored in logger.ini

  The use of this applications requires that it has access to advantage either through ADS or ALS


  VERY IMPORTANT!: The define named Production must be enabled in order for the FTP to take place
                   This define is normally commented out during testing.

}
program gmSCD;
(*)
from copy of HomeNetGF
gmSCD
Z:\E\data\source\Internet Feeds DpsVSeries\gmSCD
Host:  ftp.esgitech.com
User:  rydell
Pass:  P8hme5R7fd#

use vinDash picures

compilesd exe to c:\Programs7.1 which is where the ini needs to reside

combine new and used

10/10/14: removed used cars from feed
(**)
{$APPTYPE CONSOLE}
{$R *.RES}

//  *****************************************  //
{$DEFINE Production}
//  *****************************************  //

uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  AbArcTyp,
  IdSMTP,
  IdAttachmentFile,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData;

Var
  FileName: String;
  S: String;
  SList: TStringList;
  F: TextFile;
  Logger: TProgressLogger;
  InventoryTableName: String;
  Headers: TStringList;

procedure Setup;
begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'gmSCD';
  DM.DataModuleInit;
  SList := TStringList.Create;
//  SList.Delimiter := Char(124);
  Headers := TStringList.Create;
//  Headers.Delimiter := Char(124);
  //ConnectionToDatabase
  DM.AdsConnection1.Connect;
  Logger.AddMsg('Running Setup');
  //Get the current inventory data
  InventoryTableName := iuGetInventoryData(DM.InventoryQuery, DM.Market);  //was OpenInventory;
  //Delete pictures from current directory
//  iuDeleteFilesFromDirectory(DM.Directory, '*.jpg', Logger);
  //GetWarrantyInfo was moved to here. It generates the DM.DifferentiatorQuery info that holds differentiator pictures and warranty descriptions
  iuGetDifferentiatorPictures(Dm.DifferentiatorQuery, DM.Market); //replaces GetWarrantyData
  Headers.Add('VIN');             // A
  Headers.Add('STOCK NUMBER');    // B
  Headers.Add('YEAR');            // C
  Headers.Add('MAKE');            // D
  Headers.Add('MODEL');           // E
  Headers.Add('MODEL CODE');      // F
  Headers.Add('TRIM');            // G
  Headers.Add('BODY STYLE');      // H
  Headers.Add('TRANSMISSION');    // I
  Headers.Add('DRIVE_TRAIN');     // J
  Headers.Add('DOORS');           // K
  Headers.Add('EXTERIOR_COLOR');  // L
  Headers.Add('INTERIOR_COLOR');  // M
  Headers.Add('MSRP');            // N
  Headers.Add('INVOICE');         // O
  Headers.Add('PRICE');           // P
  Headers.Add('CITY MPG');        // Q
  Headers.Add('HIGHWAY MPG');     // R
  Headers.Add('HORSE_POWER');     // S
  Headers.Add('MILEAGE');         // T
  Headers.Add('TYPE');            // U
  Headers.Add('CREATE');          // V
  Headers.Add('LAST_MODIFIED');   // W
  Headers.Add('IMAGE URL');       // X
  Headers.Add('CERTIED');         // Y
  Headers.Add('MISC_PRICE');      // Z
end;  // Proc Setup


// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  logger.AddMsg('Cleaning up objects...');
  DM.Free;
  SList.Free;
  Headers.Free;
  logger.AddMsg('Finished program.');
end;

var
  StockNumberFld: TField;
  VINFld: TField;
  YearFld: TField;
  MakeFld: TField;
  ModelFld: TField;
  ModelCodeFld: TField;
  TrimFld: TField;
  BodyStyleFld: TField;
  TransmissionFld: TField;
  DriveTrainFld: TField;
  DoorsFld: TField;
  ExteriorColorFld: TField;
  InteriorColorFld: TField;
  MsrpFld: TField;
  InvoiceFld: TField;
  PriceFld: TField;
  MileageFld: TField;
  TypeFld: TField;
  CertifiedFld: TField;
  DiffRecNo: Integer;
  PicURLString: String;
  WarrantyString: String;
  OptionsString: String;
  VehicleItemIdFld: TField;
  VehicleInventoryItemIdFld: TField;
  OwningLocationIDFld: TField;
  ReconPackageFld: TField;

// Unit begin
begin
  Logger := TProgressLogger.create('gmSCD');
try
//  DeleteCurrentPicsFromDirectory;
  Setup;
  FileName := 'G1RYD3270W4S.csv';  //  GF specific code
  AssignFile(F, FileName);
  if FileExists(FileName) then
    DeleteFile(FileName);
  ReWrite(F);

  WriteLn(F, Headers.CommaText);
(*)
  with DM.InventoryQuery do
  begin

    //Initialize the TField references
    StockNumberFld := FieldByName('StockNumber');
    YearFld := FieldByName('Year');
    MakeFld := FieldByName('Make');
    ModelFld := FieldByName('Model');
    TrimFld := FieldByName('TrimLevel');
    BodyStyleFld := FieldbyName('BodyStyle');
    VINFld := FieldByName('VIN');
    MileageFld := FieldByName('Mileage');
    PriceFld := FieldByName('Price');
    ExteriorColorFld := FieldByName('Color');
    InteriorColorFld := FieldByName('InteriorColor');
    TransmissionFld := FieldByName('Transmission');
    OwningLocationIDFld := FieldByName('OwningLocationID');
    ReconPackageFld := FieldByName('ReconPackage');
    VehicleInventoryItemIDFld := FieldByName('VehicleInventoryItemId');
    VehicleItemIdFld := FieldByName('VehicleItemId');
    First;
    while not Eof do
    begin

      //Get the list of pictures for this vehicle
      iuGetPicturesForVehicleInventoryItemID(DM.PicsQuery, InventoryTableName, VehicleInventoryItemIDFld.AsString);
      //Position the cursor on the correct Differentiator record for the location/package/make
      DiffRecNo := iuGetDiffRecordNo(DM.DifferentiatorQuery, OwningLocationIDFld.AsString, ReconPackageFld.AsString, MakeFld.AsString);
// use vindash
      PicURLString := iuGetPicURLs(DM.PicsQuery, DM.DifferentiatorQuery, 'http://67.135.158.39/files/ImagesVinDash/', char(124), 0);
//not needed
//      WarrantyString := iuGetWarrantyString(DM.DifferentiatorQuery, DiffRecNo);
//      OptionsString := iuGetOptionsString(DM.AdsQuery1,VehicleItemIdFld.AsString);
      SList.Clear;
      //TODO : Update this code. Is it similar to what is seen in other units?
      SList.Add(VINFld.AsString);
      SList.Add(StockNumberFld.AsString);
      SList.Add(YearFld.AsString);
      SList.Add(MakeFld.AsString);
      SList.Add(ModelFld.AsString);
      SList.Add('');  // F ModelCode
      SList.Add(TrimFld.AsString);
      SList.Add(BodyStyleFld.AsString);
      SList.Add(TransmissionFld.AsString);
      SList.Add('');  // J DriveTrain
      SList.Add('');  // K Doors
      SList.Add(ExteriorColorFld.AsString);
      SList.Add(InteriorColorFld.AsString);
      SList.Add('');  // N MSRP
      SList.Add('');  // O Invoice
      SList.Add(PriceFld.AsString);
      SList.Add('');  // Q CityMpg
      SList.Add('');  // R HighwayMpg
      SList.Add('');  // S HorsePower
      SList.Add(MileageFld.AsString);
      SList.Add('U');
      SList.Add('');  // V Create
      SList.Add('');  // W LastModified
      SList.Add(PicURLString);
      if ReconPackageFld.AsString = 'ReconPackage_Factory' then  // Y certified
        SList.Add('1')
      else
        SList.Add('0');
      SList.Add('');  // Z MiscPrice
      S := SList.CommaText;
      WriteLn(F, S);
      Next;
    end;  // while not eof
  end; // with
(**)
  DM.InventoryQuery.Close;
  DM.InventoryQuery.SQL.Text :=
    'SELECT imvin, imstk#, imyear, immake, immcode, ' +
    '  immodl, imbody, imcolr, imodom, ' +
    '  cast(msrp AS sql_double) AS msrp, round(cast(invoice AS sql_double),0) AS invoice, ' +
    '  coalesce(bestprice,0) + coalesce(rebate,0) AS price ' +
    'FROM ( ' +
    '  SELECT a.imvin,a.imstk#,a.imyear,a.immake,a.immcode,a.immodl,a.imbody,a.imcolr, ' +
    '    a.imodom,a.impric AS msrp, ' +
    '    sum(CASE WHEN b.iotype = ''c'' AND b.ioseq# = 5 THEN ionval END) AS invoice, ' +
    '    sum(CASE WHEN b.iotype = ''n'' AND b.ioseq# = 9 THEN ionval END) AS bestprice, ' +
    '    sum(CASE WHEN b.iotype = ''n'' AND b.ioseq# = 12 THEN ionval END) AS rebate ' +
    '  FROM dds.stgarkonainpmast a ' +
    '  INNER JOIN dds.stgArkonainpoptf b ON a.imvin = b.iovin ' +
    '  WHERE a.imtype = ''n'' ' +
    '    AND a.imstat = ''i'' ' +
    '    AND LEFT(a.imstk#,1) <> ''H'' ' +
    '  GROUP BY a.imvin,a.imstk#,a.imyear,a.immake,a.immcode,a.immodl,a.imbody,a.imcolr, ' +
    '    a.imodom,a.impric) x';
  DM.InventoryQuery.Open;
  with DM.InventoryQuery do
  begin
    VINFld := FieldByName('imvin');
    StockNumberFld := FieldByName('imstk#');
    YearFld := FieldByName('imyear');
    MakeFld := FieldByName('immake');
    ModelCodeFld := FieldByName('immcode');
    ModelFld := FieldByName('immodl');
    BodyStyleFld := FieldbyName('imbody');
    ExteriorColorFld := FieldByName('imcolr');
    MileageFld := FieldByName('imodom');
    MsrpFld := FieldByName('msrp');
    InvoiceFld := FieldByName('invoice');
    PriceFld := FieldByName('price');
    First;
    while not Eof do
    begin
      SList.Clear;
      SList.Add(VINFld.AsString);
      SList.Add(StockNumberFld.AsString);
      SList.Add(YearFld.AsString);
      SList.Add(MakeFld.AsString);
      SList.Add(ModelFld.AsString);
      SList.Add(ModelCodeFld.AsString);  // F ModelCode
      SList.Add('');  // G Trim
      SList.Add(BodyStyleFld.AsString);
      SList.Add('');  //I Transmission
      SList.Add('');  // J DriveTrain
      SList.Add('');  // K Doors
      SList.Add(ExteriorColorFld.AsString);
      SList.Add('');  // M Int Color
      SList.Add(MsrpFld.AsString);
      SList.Add(InvoiceFld.AsString);
      SList.Add(PriceFld.AsString);
      SList.Add('');  // Q CityMpg
      SList.Add('');  // R HighwayMpg
      SList.Add('');  // S HorsePower
      SList.Add(MileageFld.AsString);
      SList.Add('N');
      SList.Add('');  // V Create
      SList.Add('');  // W LastModified
      SList.Add('');  //X ImageURL
      SList.Add('');  // Y certified
      SList.Add('');  // Z MiscPrice
      S := SList.CommaText;
      WriteLn(F, S);
      Next;
    end;
  end;
  CloseFile(F);
  logger.AddMsg('Finished creating inventory file.');
(**)
{$IFDEF Production}
  iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, 'h9rxdnySLwgY', FileName, FileName, Logger);
{$ENDIF}
  iuLogFileContents(FileName, Logger);
  iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
    'GM SCD',
    'The Rydell GM Autocenter inventory file has been sent to the GM SCD ftp site.',
     DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
(**)
  Shutdown;
  except
    on e:eAdsDataBaseError do
    begin
      Logger.AddError(e.ACEErrorCode, e.Message);
      Logger.StoreLog;
    end;
    on e:Exception do
    begin
      Logger.AddError(1000, e.Message);
      Logger.StoreLog;
    end;
  end;
//  Logger.Free;
end.
