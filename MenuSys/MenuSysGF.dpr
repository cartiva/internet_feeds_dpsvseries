{This program relies on two external files and an external Advantage table

  appname.ini (where appname is the name of this project)
    This file contains the valid markets, locatoin, the connection string, notification email addresses,
    ftp address and user name and directories where data will be written.

  logger.ini
    This file contains the connection information for a free Advantage table that will hold the log for this run

  External Advantage table
    This file contains the log for the run. Connection information is stored in logger.ini

  The use of this applications requires that it has access to advantage either through ADS or ALS


  VERY IMPORTANT!: The define named Production must be enabled in order for the FTP to take place
                   This define is normally commented out during testing.

}
program MenuSysGF;
(*)
   copied HomeNetGF to do this new car version
(**)
{$APPTYPE CONSOLE}
{$R *.RES}

// *********************************** //
{$DEFINE Production}
// *********************************** //


uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  IdSMTP,
  IdAttachmentFile,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData,
  ADODB;

Var
  FileName: String;
  S: String;
  SList: TStringList;
  F: TextFile;
  Logger: TProgressLogger;
  Headers: TStringList;

procedure Setup;
begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'MenuSysGF';
  DM.DataModuleInit;
  SList := TStringList.Create;
  Headers := TStringList.Create;
  //ConnectionToDatabase
  DM.AdsConnection1.Connect;
  Logger.AddMsg('Running Setup');

  Headers.Add('DEALERKEY');
  Headers.Add('STOCKNUMBER');
  Headers.Add('VIN');
  Headers.Add('YEAR');
  Headers.Add('MAKE');
  Headers.Add('MODEL');
  Headers.Add('N/U');
  Headers.Add('MILES');
  Headers.Add('COST');
  Headers.Add('PRICE');
end;  // Proc Setup


// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  logger.AddMsg('Cleaning up objects...');
  DM.Free;
  SList.Free;
  Headers.Free;
  logger.AddMsg('Finished program.');
end;

var
  StockNumberFld: TField;
  VINFld: TField;
  YearFld: TField;
  MakeFld: TField;
  ModelFld: TField;
  NewUsedFld: TField;
  MileageFld: TField;
  CostFld: TField;
  PriceFld: TField;
  ADOCon: TADOConnection;
  ADOQuery: TADOQuery;
// Unit begin
begin
  Logger := TProgressLogger.create('MenuSysGF');
try
//  DeleteCurrentPicsFromDirectory;
  Setup;
  FileName := 'MenuSysRydInv' + '.csv';  //  GF specific code
  FileName := StringReplace(FileName, '/', '', [rfReplaceAll]);
  AssignFile(F, FileName);
  if FileExists(FileName) then
    DeleteFile(FileName);
  ReWrite(F);
  try
    ADOCon := TADOConnection.Create(nil);
    ADOQuery := TADOQuery.Create(nil);
    ADOCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
    ADOQuery.SQL.Add(
      'select trim(imstk#) as StockNumber, imvin as VIN, imyear as Year, trim(immake) as Make, ' +
      'trim(immodl) as Model, imtype as "N/U", imodom as Miles, imcost as Cost, impric as Price ' +
      'from inpmast where imstat = ''I''');
    ADOCon.Connected := True;
    WriteLn(F, Headers.CommaText);
    ADOQuery.Connection := ADOCon;
    ADOQuery.Open;

    with ADOQuery do
    begin
      //Initialize the TField references
      StockNumberFld := FieldByName('StockNumber');
      VINFld := FieldByName('VIN');
      YearFld := FieldByName('YEAR');
      MakeFld := FieldByName('MAKE');
      ModelFld := FieldByName('MODEL');
      NewUsedFld := FieldByName('N/U');
      MileageFld := FieldByName('Miles');
      CostFld := FieldByName('Cost');
      PriceFld := FieldByName('Price');

      First;
      while not Eof do
      begin
        SList.Clear;
        SList.Add('f3429528-9e83-40cb-81b1-070f305500f1');
        SList.Add(StockNumberFld.AsString);
        SList.Add(VINFld.AsString);
        SList.Add(YearFld.AsString);
        SList.Add(MakeFld.AsString);
        SList.Add(ModelFld.AsString);
        SList.Add(NewUsedFld.AsString);
        SList.Add(MileageFld.AsString);
        SList.Add(CostFld.AsString);
        SList.Add(PriceFld.AsString);
        S := SList.CommaText;
        S := StringReplace(S, '"', '',[rfReplaceAll]);
        WriteLn(F, S);
        Next;
      end;  // while not eof
    end; // with
  finally
    ADOCon.Free;
    ADOQuery.Free;
  end;
  CloseFile(F);
  logger.AddMsg('Finished creating inventory file.');
(**)
{$IFDEF Production}
//  iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, 'gx8m6', FileName, FileName, Logger);
  iuFtpFile(DM.FTP, 'download.cartiva.com', 'download', 'th!s!sc@rt!v@ftp', FileName, FileName, Logger);
{$ENDIF}
  iuLogFileContents(FileName, Logger);
  iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
    'MenuSys Grand Forks',
    'The Rydell GM Autocenter new inventory file has been sent to MenuSys.',
     DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
(**)
  Shutdown;
  except
    on e:eAdsDataBaseError do
    begin
      Logger.AddError(e.ACEErrorCode, e.Message);
      Logger.StoreLog;
    end;
    on e:Exception do
    begin
      Logger.AddError(1000, e.Message);
      Logger.StoreLog;
    end;
  end;
//  Logger.Free;
end.
