{This program relies on two external files and an external Advantage table

  appname.ini (where appname is the name of this project)
    This file contains the valid markets, locatoin, the connection string, notification email addresses,
    ftp address and user name and directories where data will be written.

  logger.ini
    This file contains the connection information for a free Advantage table that will hold the log for this run

  External Advantage table
    This file contains the log for the run. Connection information is stored in logger.ini

  The use of this applications requires that it has access to advantage either through ADS or ALS


  VERY IMPORTANT!: The define named Production must be enabled in order for the FTP to take place
                   This define is normally commented out during testing.

}
program CarsComGF;
(*
2006-05-10:
  2.1.0.0
    removed call to iuFtpFile, restored old code that FTP' each individual
    picture
2006-06-12
  2.2.0.0
    FTP multiple pictures of each vehicle
    was only Front 3/4 view
2006-07-17
  2.3.0.0
    Turns out Cars.com wants pictures named by stocknumber, i.e. L123Z_1_of_3.jpg
    Looks like I need to redo beaucoup in this fucker
    had to abandon iuSave

v2.3.0.1
  07/31/2006
  changed picture query to: Where UsedCars.VIN = VehiclePictures.picvin
                      from: Where UsedCars.VehicleID = VehiclePictures.VehicleID
  when clean up is run, everything except sold & wholesaled gets deleted, so that
  when subsequently updated, UsedCars.VehicleID get a new value

v2.4
  2007-03-25
    added InternetComment
v2.4.0.3
  2008-01-02
    added ReconPackage 6
v2.4.0.6
  2008-02-12
    add PkgSaturnCert
v2.4.0.7
  2008-05-15
  and ((SalesStatus = 'Available') or (ShowOnWebsite = 'yes' and SalesStatus not in ('Sold','Wholesaled')))
v2.4.0
  2008-06-01
    copy of CarsComINV for Saturn of Mounds View
v2.4.0.
  06/22/2008
  eliminate temp stock numbers
  eliminate zip file

v1.0.0.4
  2008-08-01
  Used as a template for CarsCom GF feed
  changed to csv
v1.0.0.10
  2008-11-17
  restarted feed
  fixed ordering of pictures (from description to ordered)
v1.0.0.12
  2008-12-24
  added Recon Buffer, Recon WIP
  fixed DecodeWarranty
v1.0.0.13
  2008-01-11
  add Cadillac Certified
v2.0.0.0
  2009-02-25
  added additional ftp feed for honda
v3.0.0.0
  2010-01-14
  Major refactoring of project. No added functionality
  Replaced the CartivaDpsBatchDM module with DpsVSeriesBatchDM
    DpsVSeriesBatchDM raises exceptions if number of parameters is incorrect (currently only accepts 1, which is an identifier for the market)
      raises an exception is the ConnectPath key is missing or empty in the configuration file
      Cleaned up significantly, removing unused declarations and code
      Raises an exception if the wrong number of parameters is passed
      Raises an exception if the parameter does not map to a valid market
  Configuration Files:
    Added a Database key in the Global section. This key contains the ConnectPath data for the AdsConnection
  Project Source File
    Includes DpsVSeriesBatchDM instead of CartivaDpsBatchDM
    Radical change to overall structure.
    Now includes five major code segments and two var declarations
    The first Var declaration includes variables available to all code segments.
    The second Var declaration includes variables used in the main body of the project file
    The First code segment is a procedure named Setup
    The Second code segment is a procedure named Shutdown
    The Third code segment is the main body of the project
    The Fourth and Fifth code segments FTP pictures to the Cars.com server
  Setup
    Creates the TDpsVSeriesBatchDM instance which contains the AdsConnection and queries that will be used
    Creates SList, which holds the string contents that are written to the file (and which is emailed)
    The creation of SList may include the definition of its delimiter, which is different for different Internet feeds
    Creates the Header TStringList, which contains the field names that are written to SList (when used). This is unique to this feed.
    Connects the database
    Creates the inventory table (all current vehicles that might have pictures), through a call to InventoryTableName
    Removes any previously created picture files from the designated directory through a call to iuDeleteFilesFromDirectory
    Gets the list of Differentiator pictures and warranty descriptions for the market through a call to iuGetDifferentiatorPictures
  Shutdown
    Frees the DpsVSeriesBatchDM module (closes the database connection)
    Frees SList
  Main Body
    This routine is the one that contains unique values for each Internet feed
    Calls Setup
    Creates a file reference (where the file contents of SList will be written, removing the old version first (if it exists)
    Initializes TField variables for each of the fields that need to be written (there are additional TFields created here which may not be used, but could be used in the future)
    Iterates over all VehicleInventoryItems in the InventoryTable
    Gets the list of pictures for this VehicleInventoryItem through a call to iuGetPicturesForVehicleInventoryItemID
    Moves the cursor of the DifferentiatorQuery to the correct record based on LocationID, ReconPackage, and Make (Factory only) through a call to iuGetDiffRecordNo
    Save pictures (if there is a recon package and pictures) for this vehicle to the defined directory through a call to iuSaveVehiclePicturesToFile
     Gets the string that describes the vehicles warranty information
    Gets the string that expands the vehicles options abbreviations
    Writes this data for the current VehicleInventoryItem
    FTPs the pictures to the Cars.com server
    EMails the SList contents
12/17/13
  *a*
  the Rydell ftp credentials have been axed (per Tony A)
  only do the honda part
*)

{$APPTYPE CONSOLE}
{$R *.RES}

{$DEFINE Production}

uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  AbArcTyp,
  IdSMTP,
  IdAttachmentFile,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData;

Var
  FileName: String;
  S: String;
  SList,
  Headers: TStringList;
  F: TextFile;
  SearchRec: TSearchRec;
  Logger: TProgressLogger;
  InventoryTableName: String;

procedure Setup;
begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'CarsComGF';
  DM.DataModuleInit;
  SList := TStringList.Create;
  Headers := TStringList.Create;
  Logger.AddMsg('Running Setup');
  //connection to database
  DM.AdsConnection1.Connect;
  //The following line had to be moved to immediately fpollowing ConnectToDatabase;
  InventoryTableName := iuGetInventoryData(DM.InventoryQuery, DM.Market);  //was OpenInventory;
  //Delete pictures from current directory
  iuDeleteFilesFromDirectory(DM.Directory, '*.jpg', Logger);
  //GetWarrantyInfo was moved to here. It generates the DM.DifferentiatorQuery info that holds differentiator pictures and warranty descriptions
  iuGetDifferentiatorPictures(Dm.DifferentiatorQuery, DM.Market); //replaces GetWarrantyData
  Headers.Add('Stock Number');
  Headers.Add('VIN');
  Headers.Add('Year');
  Headers.Add('Make');
  Headers.Add('Model');
  Headers.Add('Trim Level');
  Headers.Add('Price');
  Headers.Add('Mileage');
  Headers.Add('Transmission');
  Headers.Add('Color');
  Headers.Add('Options');  // Warranty info & Options
  Headers.Add('Warranty');
end;  // Proc Setup

  // Puts files on FTP server
//*a*
//procedure FTPFileRydell;
//begin
//  logger.AddMsg('Logging onto FTP site...');
//  with DM.FTP do
//  begin
//// CarSoup FTP specific code
//
//    Host := 'ftp.dmotorworks.com';
//    UserName := 'DLR158651';
//    Password := 'karly160';
//    Connect;
//    TransferType := ftBinary;
//  end;
//  logger.AddMsg('Putting files on FTP server...');
//  // iterates through images folder to send image to ftp server
//    if FindFirst(DM.Directory + '*.jpg', faAnyFile, SearchRec) = 0 then
//    repeat
//      DM.FTP.Put(DM.Directory + SearchRec.Name, SearchRec.Name);
//    until FindNext(SearchRec) <> 0;
//    FindClose(SearchRec);
//    DM.FTP.Put(Filename, 'CarsComGF.txt');
//    DM.FTP.Abort;
//    DM.FTP.Quit;
//    logger.AddMsg('Finished putting files on FTP server...');
// end;

  // Puts files on FTP server
procedure FTPFileHonda;
begin
  logger.AddMsg('Logging onto FTP site...');
  with DM.FTP do
  begin
    Host := 'ftp.dmotorworks.com';
    UserName := 'DLR158647';
    Password := 'ruthy92';
    Connect;
    TransferType := ftBinary;
  end;
  logger.AddMsg('Putting files on FTP server...');
  // iterates through images folder to send image to ftp server
    if FindFirst(Dm.Directory + '*.jpg', faAnyFile, SearchRec) = 0 then
    repeat
      DM.FTP.Put(DM.Directory + SearchRec.Name, SearchRec.Name);
    until FindNext(SearchRec) <> 0;
    FindClose(SearchRec);
    DM.FTP.Put(Filename, 'CarsComGF.txt');
    DM.FTP.Abort;
    DM.FTP.Quit;
    logger.AddMsg('Finished putting files on FTP server...');
 end;

// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  logger.AddMsg('Cleaning up objects...');
  DM.Free;
  SList.Free;
  Headers.Free;
end;

var
  StockNumberFld: TField;
  YearFld: TField;
  MakeFld: TField;
  ModelFld: TField;
  TrimLevelFld: TField;
  VINFld: TField;
  MileageFld: TField;
  PriceFld: TField;
  ColorFld: TField;
  InteriorColorFld: TField;
  EngineFld: TField;
  TransmissionFld: TField;
  OwningLocationIDFld: TField;
  ReconPackageFld: TField;
  InternetCommentFld: TField;
  VehicleInventoryItemIDFld: TField;
  VehicleItemIdFld: TField;
  DiffRecNo: Integer;
  PicURLString: String;
  WarrantyString: String;
  OptionsString: String;

// Unit begin
begin
  Logger := TProgressLogger.create('CarsComGF');
try
  Setup;
  FileName := 'CarsComGF.txt';
  AssignFile(F, FileName);
  if FileExists(FileName) then
    DeleteFile(FileName);
  ReWrite(F);

  WriteLn(F, Headers.CommaText);

  with DM.InventoryQuery do
  begin

    //Initialize the TField references
    StockNumberFld := FieldByName('StockNumber');
    YearFld := FieldByName('Year');
    MakeFld := FieldByName('Make');
    ModelFld := FieldByName('Model');
    TrimLevelFld := FieldByName('TrimLevel');
    VINFld := FieldByName('VIN');
    MileageFld := FieldByName('Mileage');
    PriceFld := FieldByName('Price');
    ColorFld := FieldByName('Color');
    InteriorColorFld := FieldByName('InteriorColor');
    EngineFld := FieldByName('Engine');
    TransmissionFld := FieldByName('Transmission');
    OwningLocationIDFld := FieldByName('OwningLocationID');
    ReconPackageFld := FieldByName('ReconPackage');
    InternetCommentFld := FieldByName('InternetComment');
    VehicleInventoryItemIDFld := FieldByName('VehicleInventoryItemId');
    VehicleItemIdFld := FieldByName('VehicleItemId');

    First;
    while not Eof do
    begin
      //Get the list of pictures for this vehicle
      iuGetPicturesForVehicleInventoryItemID(DM.PicsQuery, InventoryTableName, VehicleInventoryItemIDFld.AsString);
      //Position the cursor on the correct Differentiator record for the location/package/make
      DiffRecNo := iuGetDiffRecordNo(DM.DifferentiatorQuery, OwningLocationIDFld.AsString, ReconPackageFld.AsString, MakeFld.AsString);
      //Save pictures for this current vehicle
      iuSaveVehiclePicturesToFile(DM.PicsQuery, DM.InventoryQuery, DM.DifferentiatorQuery, Dm.Directory, 'Picture', 5, Logger);

       //Get additional data
      WarrantyString := iuGetWarrantyString(DM.DifferentiatorQuery, DiffRecNo);
      OptionsString := iuGetOptionsString(DM.AdsQuery1,VehicleItemIdFld.AsString);
      Write(StockNumberFld.AsString + ' - ');
      SList.Clear;
      SList.Add(StringReplace(StockNumberFld.AsString, '*', '', [rfReplaceAll]));
      SList.Add(VINFld.AsString);
      SList.Add(YearFld.AsString);
      SList.Add(MakeFld.AsString);
      SList.Add(ModelFld.AsString);
      SList.Add(TrimLevelFld.AsString);
      SList.Add(PriceFld.AsString);
      SList.Add(MileageFld.AsString);
      SList.Add(TransmissionFld.AsString);
      SList.Add(ColorFld.AsString);
      SList.Add(OptionsString);
      SList.Add(WarrantyString);
      S := SList.CommaText;
      WriteLn(F, S);
      Next;
    end;  // while not eof
  end; // with
  CloseFile(F);
  logger.AddMsg('Finished creating inventory file.');
{$IFDEF Production}
//*a*
//  FTPFileRydell;
  FTPFileHonda;
{$ENDIF}
  iuLogFileContents(FileName, Logger);
{$IFDEF Production}
  iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
      'Rydell GM Autocenter Inventory & Pics - Cars.com files sent to FTP Server',
      'Rydell GM Autocenter inventory & image files have been sent to the Cars.com ftp site.',
      DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
{$ENDIF}
  Shutdown;
  except on e:EAdsDataBaseError do
  begin
    Logger.AddError(e.ACEErrorCode, e.Message);
    Logger.StoreLog;
  end;
end;
//Logger.Free;
end.
