{This program relies on two external files and an external Advantage table

  appname.ini (where appname is the name of this project)
    This file contains the valid markets, locatoin, the connection string, notification email addresses,
    ftp address and user name and directories where data will be written.

  logger.ini
    This file contains the connection information for a free Advantage table that will hold the log for this run

  External Advantage table
    This file contains the log for the run. Connection information is stored in logger.ini

  The use of this applications requires that it has access to advantage either through ADS or ALS


  VERY IMPORTANT!: The define named Production must be enabled in order for the FTP to take place
                   This define is normally commented out during testing.

}
program NissanCPO;
(*)
2/8/13
copied from HomeNetGF
Nissan Certified:
  no pictures
ftp.dmotorworks.com
UN: dp45054
PW: darcy670
1. replace HomeNet text with NissanCPO
2. comment out Production directive
4/21/13
  got file spec from nissan (digital motorworks)
  shit load of blank fields
  no headers
  dlr # 3071
(**)
{$APPTYPE CONSOLE}
{$R *.RES}

//  ****************************************  //
{$DEFINE Production}
//  ****************************************  //

uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  AbArcTyp,
  IdSMTP,
  IdAttachmentFile,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData;

Var
  FileName: String;
  S: String;
  SList: TStringList;
  F: TextFile;
  Logger: TProgressLogger;
  InventoryTableName: String;
  OptionsString: String;
//  Headers: TStringList;

procedure Setup;
begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'NissanCPO';
  DM.DataModuleInit;
  SList := TStringList.Create;
  DM.AdsConnection1.Connect;
  Logger.AddMsg('Running Setup');
end;  // Proc Setup

// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  logger.AddMsg('Cleaning up objects...');
  DM.Free;
  SList.Free;
//  Headers.Free;
  logger.AddMsg('Finished program.');
end;

// Unit begin
begin
  Logger := TProgressLogger.create('NissanCPO');
try
  Setup;

  DM.InventoryQuery.Close;
  DM.InventoryQuery.SQL.Text :=
    'SELECT a.stocknumber, b.vin, b.yearmodel, b.make, b.model, b.TRIM, ' +
    '  b.bodystyle, b.exteriorcolor, b.interiorcolor, b.engine, b.transmission, ' +
    '  d.value, f.amount, g.vehicleoptions, b.VehicleItemID ' +
    'FROM VehicleInventoryItems a ' +
    'INNER JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID ' +
    'INNER JOIN SelectedReconPackages c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID ' +
    '  AND c.typ = ''ReconPackage_Factory'' ' +
    '  AND c.SelectedReconPackageTS = ( ' +
    '    SELECT MAX(SelectedReconPackageTS) ' +
    '    FROM SelectedReconPackages  ' +
    '    WHERE VehicleInventoryItemID = c.VehicleInventoryItemID  ' +
    '    GROUP BY VehicleInventoryItemID) ' +
    '  LEFT JOIN VehicleItemMileages d ON a.VehicleItemID = d.VehicleItemID  ' +
    '  AND d.VehicleItemMileageTS = ( ' +
    '    SELECT MAX(VehicleItemMileageTS) ' +
    '    FROM VehicleItemMileages ' +
    '   WHERE VehicleItemID = d.VehicleItemID ' +
    '   GROUP BY VehicleItemID) ' +
    '  LEFT JOIN VehiclePricings e ON a.VehicleInventoryItemID = e.VehicleInventoryItemID ' +
    '  AND VehiclePricingTS = ( ' +
    '    SELECT MAX(VehiclePricingTS) ' +
    '    FROM VehiclePricings ' +
    '    WHERE VehicleInventoryItemID = e.VehicleInventoryItemID ' +
    '    GROUP BY VehicleInventoryItemID) ' +
    '  LEFT JOIN VehiclePricingDetails f ON e.VehiclePricingID = f.VehiclePricingID ' +
    '  AND f.typ = ''VehiclePricingDetail_BestPrice'' ' +
    '  LEFT JOIN VehicleItemOptions g ON a.VehicleItemID = g.VehicleItemID ' +
    '  WHERE a.thruts IS NULL ' +
    '    AND make = ''nissan'' ' +
    '    AND amount is not null';
  DM.InventoryQuery.Open;
  FileName := 'vehicle_inventory.txt';
  AssignFile(F, FileName);
  if FileExists(FileName) then
    DeleteFile(FileName);
  ReWrite(F);

  with DM.InventoryQuery do
  begin
    First;
    while not Eof do
    begin
// no pictures
      OptionsString := iuGetOptionsString(DM.AdsQuery1,FieldByName('VehicleItemID').AsString, CHAR(124));
      SList.Clear;
      //TODO : Update this code. Is it similar to what is seen in other units?
      SList.Add('3071');
      SList.Add(FieldByName('StockNumber').AsString);
      SList.Add('Used');
      SList.Add(''); // inventory date
      SList.Add(''); // days in inventory
      SList.Add(''); // invoice amount
      SList.Add(''); // pack amount
      SList.Add(''); // cost
      SList.Add(FieldByName('Amount').AsString); // list price
      SList.Add(''); // msrp
      SList.Add(''); // lot location
      SList.Add(''); // status
      SList.Add('Yes'); // certified
      SList.Add(FieldByName('vin').AsString); // vin
      SList.Add(FieldByName('value').AsString); // odometer
      SList.Add(FieldByName('yearmodel').AsString); // year
      SList.Add(FieldByName('make').AsString); // make
      SList.Add(FieldByName('model').AsString); // model
      SList.Add(FieldByName('trim').AsString); // trim level
      SList.Add(''); // type code
      SList.Add(FieldByName('bodystyle').AsString); // body style
      SList.Add(''); // classification
      SList.Add(''); // payload capacity
      SList.Add(''); // seating capacity
      SList.Add(''); // wheel base
      SList.Add(''); // drive train
      SList.Add(FieldByName('engine').AsString); // engine
      SList.Add(FieldByName('exteriorcolor').AsString); // exterior color
      SList.Add(FieldByName('interiorcolor').AsString); // interior deescription
      SList.Add(FieldByName('transmission').AsString); // transmission
      SList.Add(OptionsString); // vehicle features
      SList.Add(''); // condition
      SList.Add(''); // tagline
      SList.Add(''); // certification number
      SList.Add(''); // holdback amount
      SList.Add(''); // license plate number
      SList.Add(''); // registration state
      SList.Add(''); // registration expiration date
      SList.Add(''); // model code
      SList.Add(''); // reconditioning cost
      SList.Add(''); // subtrim level

      S := SList.CommaText;
      WriteLn(F, S);
      Next;
    end;  // while not eof
  end; // with
  CloseFile(F);
  logger.AddMsg('Finished creating inventory file.');
{$IFDEF Production}
  iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, 'darcy670', FileName, FileName, Logger);
{$ENDIF}
  iuLogFileContents(FileName, Logger);
{$IFDEF Production}
  iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
    'NissanCPO Used Grand Forks',
    'The Rydell GM Autocenter used inventory file has been sent to the NissanCPO ftp site.',
     DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
{$ENDIF}
  Shutdown;
  except
    on e:eAdsDataBaseError do
    begin
      Logger.AddError(e.ACEErrorCode, e.Message);
      Logger.StoreLog;
    end;
    on e:Exception do
    begin
      Logger.AddError(1000, e.Message);
      Logger.StoreLog;
    end;
  end;
//  Logger.Free;
end.
