program GmacViwGF;
(*
TODO: remove nice car pics
      remove duplicate records

v1.1.0.44
  2007-08-23
  removed FTP code - will need to do it manually for now, though there is
  promise of doing it from the command line

05/17/2011
  time to rescucitate this dog
  refactored to match the Cary mod for DpsVSeries
  based on the Dealer.com feed

/******************************************************************************/
this feed is different
run manually, locally (non PRODUCTION) to generate the feed file and manually ftp the file to the
ftp site:   ftp.inspections.ally.com
            FTPES - FTP over explicit TLS/SSL (Filezilla)
            user: IV_RYDELL
            password: viw_0607a
then run the app on the server (PRODUCTION) to populate the pictures directory
/******************************************************************************/
(**)

{$APPTYPE CONSOLE}

{$R *.RES}

//{$DEFINE PRODUCTION}

uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  AbArcTyp,
  IdSMTP,
  IdAttachmentFile,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData;

Var
  FileName: String;
  S: String;
  SList: TStringList;
  Headers: TStringList;
  F: TextFile;
  Logger: TProgressLogger;
  InventoryTableName: String;

procedure Setup;
begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'GmacVIWGF';
  DM.DataModuleInit;
  Logger.AddMsg('Created Data Module');
  SList := TStringList.Create;
  Logger.AddMsg('Created Objects');
  DM.AdsConnection1.Connect;
  Logger.AddMsg('Connected to Database');
  ////Get the current inventory data
  InventoryTableName := iuGetInventoryData(DM.InventoryQuery, DM.Market);  //was OpenInventory;
  ////Delete pictures from current directory
  iuDeleteFilesFromDirectory(DM.Directory, '*.jpg', Logger);
  ////GetWarrantyInfo was moved to here. It generates the DM.DifferentiatorQuery info that holds differentiator pictures and warranty descriptions
  iuGetDifferentiatorPictures(Dm.DifferentiatorQuery, DM.Market); //replaces GetWarrantyData

end;
//// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  DM.Free;
  SList.Free;
  DM.AdsConnection1.Disconnect;
  Logger.AddMsg('Finished program.');
end;

var
  StockNumberFld: TField;
  YearFld: TField;
  MakeFld: TField;
  ModelFld: TField;
  TrimLevelFld: TField;
  VINFld: TField;
  MileageFld: TField;
  ColorFld: TField;
  InteriorColorFld: TField;
  TransmissionFld: TField;
  VehicleInventoryItemIDFld: TField;
  VehicleItemIdFld: TField;
  OwningLocationIDFld: TField;
  ReconPackageFld: TField;
  DiffRecNo: Integer;
  PicURLString: String;
  WarrantyString: String;
  OptionsString: String;
  InventoryCount: String;

// Unit begin
begin
  Logger := TProgressLogger.Create('GmacViwGF');
  try
    Logger.AddMsg('Running Setup');
    Setup;
    FileName := 'InventoryFile_' + FormatDateTime('mmddyyyyhhnn', Now()) + '.dat';
    AssignFile(F, FileName);
    if FileExists(FileName) then
      DeleteFile(FileName);
    ReWrite(F);

    with DM.InventoryQuery do
    begin
      InventoryCount := IntToStr(DM.InventoryQuery.RecordCount);
      SList.Delimiter := Char(124);
      SList.Add('VIWHEADER');
      SList.Add(FormatDateTime('mmddyyy:hh:nn', Now()));
      Slist.Add(InventoryCount);
      WriteLn(F, SList.DelimitedText);
      SList.Clear;
      ////Initialize the TField references
      StockNumberFld := FieldByName('StockNumber');
      YearFld := FieldByName('Year');
      MakeFld := FieldByName('Make');
      ModelFld := FieldByName('Model');
      TrimLevelFld := FieldByName('TrimLevel');
      VINFld := FieldByName('VIN');
      MileageFld := FieldByName('Mileage');
      ColorFld := FieldByName('Color');
      InteriorColorFld := FieldByName('InteriorColor');
      TransmissionFld := FieldByName('Transmission');
      VehicleInventoryItemIDFld := FieldByName('VehicleInventoryItemId');
      VehicleItemIdFld := FieldByName('VehicleItemId');
      ReconPackageFld := FieldByName('ReconPackage');
      OwningLocationIDFld := FieldByName('OwningLocationID');
      First;
      while not Eof do
      begin

        //Get the list of pictures for this vehicle
        iuGetPicturesForVehicleInventoryItemID(DM.PicsQuery, InventoryTableName, VehicleInventoryItemIDFld.AsString);
        //Position the cursor on the correct Differentiator record for the location/package/make
        DiffRecNo := iuGetDiffRecordNo(DM.DifferentiatorQuery, OwningLocationIDFld.AsString, ReconPackageFld.AsString, MakeFld.AsString);
        //Save pictures for this current vehicle
{$IFDEF Production}
        iuSaveVehiclePicturesToFile(DM.PicsQuery, DM.InventoryQuery, DM.DifferentiatorQuery, Dm.Directory, 'Picture', 0, Logger);
{$ENDIF}
        PicURLString := iuGetPicURLs(DM.PicsQuery, DM.DifferentiatorQuery, 'http://67.135.158.39/files/imagesGMACVIW/', Char(126), 0);
        OptionsString := iuGetOptionsString(DM.AdsQuery1,VehicleItemIdFld.AsString, Char(126));
        SList.Clear;
        SList.Delimiter := Char(124);
        SList.Add('RECORD');
        SList.Add(VINFld.AsString);
        SList.Add(StockNumberFld.AsString);
        SList.Add(YearFld.AsString);
        SList.Add(MakeFld.AsString);
        SList.Add(ModelFld.AsString);
        SList.Add(TrimLevelFld.AsString);
        SList.Add(MileageFld.AsString);
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add(TransmissionFld.AsString);
        SList.Add(ColorFld.AsString);
        SList.Add(InteriorColorFld.AsString);
        SList.Add('');
        SList.Add('');
        SList.Add(''); // Comments
        SList.Add('');
        SList.Add('Rydell GM AutoCenter');
        SList.Add(''); //DealerContact
        SList.Add('2700 S Washington');
        SList.Add('');
        SList.Add('Grand Forks');
        SList.Add('58201');
        SList.Add('ND');
        SList.Add('701-780-1380');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
        SList.Add('');
       S := SList.DelimitedText;
       S := StringReplace(S, '"','',[rfReplaceAll, rfIgnoreCase]);
       WriteLn(F, S);

       SList.Clear;
         if OptionsString <> '' then
         begin
           SList.Add('ACCESSORY');
           SList.Add(VINFld.AsString);
           SList.Add(OptionsString);
           S := SList.DelimitedText;
           S := StringReplace(S, '"','',[rfReplaceAll, rfIgnoreCase]);
           WriteLn(F, S);
         end;

       SList.Clear;
        SList.Add('IMAGE'); /// seems like this should be conditional
        SList.Add(VINFld.AsString);
        if PicURLString <> '' then
        begin
          SList.Add(PicURLString);    // Image URL List
          S := SList.DelimitedText;
          WriteLn(F, S);
        end;

        Next;
      end;  // while not eof
    end; // with

    CloseFile(F);
    Logger.AddMsg('Finished creating inventory file.');
    iuLogFileContents(FileName, Logger);
{$IFDEF Production}
// this feeed is different, not automated until i can figure out how to do
// FTP over SSL in code
// only run occasionally, on demand, no need to email anybody
(*)
    iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, 'E23h46vp', FileName, FileName, Logger);
    iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
      'GF Dealer.com feed',
      'The Grand Forks inventory file has been sent to the Dealer.com ftp site.',
      DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
(**)
{$ENDIF}


    Shutdown;
  except
    on e:eAdsDataBaseError do
    begin
      Logger.AddError(e.ACEErrorCode, e.Message);
      Logger.StoreLog;
    end;
    on e:Exception do
    begin
      Logger.AddError(1000, e.Message);
      Logger.StoreLog;
    end;
  end;
end.

