{This program relies on two external files and an external Advantage table

  appname.ini (where appname is the name of this project)
    This file contains the valid markets, locatoin, the connection string, notification email addresses,
    ftp address and user name and directories where data will be written.

  logger.ini
    This file contains the connection information for a free Advantage table that will hold the log for this run

  External Advantage table
    This file contains the log for the run. Connection information is stored in logger.ini

  The use of this applications requires that it has access to advantage either through ADS or ALS


  VERY IMPORTANT!: The define named Production must be enabled in order for the FTP to take place
                   This define is normally commented out during testing.

}
program CobaltGrandForksPhotos;
(*)
2009-11-07
  v1.0.0.98
  updated to D2009
v3.0.0.0
  2010-01-14
  Major refactoring of project. No added functionality
  Replaced the CartivaDpsBatchDM module with DpsVSeriesBatchDM
    DpsVSeriesBatchDM raises exceptions if number of parameters is incorrect (currently only accepts 1, which is an identifier for the market)
      raises an exception is the ConnectPath key is missing or empty in the configuration file
      Cleaned up significantly, removing unused declarations and code
      Raises an exception if the wrong number of parameters is passed
      Raises an exception if the parameter does not map to a valid market
  Configuration Files:
    Added a Database key in the Global section. This key contains the ConnectPath data for the AdsConnection
  Project Source File
    Includes DpsVSeriesBatchDM instead of CartivaDpsBatchDM
    Radical change to overall structure.
    Now includes three major code segments and two var declarations
    The first Var declaration includes variables available to all code segments.
    The second Var declaration includes variables used in the main body of the project file
    The First code segment is a procedure named Setup
    The Second code segment is a procedure named Shutdown
    The Third code segment is the main body of the project
  Setup
    Creates the TDpsVSeriesBatchDM instance which contains the AdsConnection and queries that will be used
    Creates SList, which holds the string contents that are written to the file (and which is emailed)
    The creation of SList may include the definition of its delimiter, which is different for different Internet feeds
    Creates the Header TStringList, which contains the field names that are written to SList (when used). This is unique to this feed.
    Connects the database
    Creates the inventory table (all current vehicles that might have pictures), through a call to InventoryTableName
    Removes any previously created picture files from the designated directory through a call to iuDeleteFilesFromDirectory
    Gets the list of Differentiator pictures and warranty descriptions for the market through a call to iuGetDifferentiatorPictures
  Shutdown
    Frees the DpsVSeriesBatchDM module (closes the database connection)
    Frees SList
  Main Body
    This routine is the one that contains unique values for each Internet feed
    Calls Setup
    Creates a file reference (where the file contents of SList will be written, removing the old version first (if it exists)
    Initializes TField variables for each of the fields that need to be written
    Iterates over all VehicleInventoryItems in the InventoryTable
    Gets the list of pictures for this VehicleInventoryItem through a call to iuGetPicturesForVehicleInventoryItemID
    Moves the cursor of the DifferentiatorQuery to the correct record based on LocationID, ReconPackage, and Make (Factory only) through a call to iuGetDiffRecordNo
    Creates a string of URLS for the pictures written through the call to iuGetPicturesForVehicleInventoryItemID
    Writes this data for the current VehicleInventoryItem to a file
    FTPs and Emails this file

v3.0.0.1
  08/2/2010:  oops, had left out iuSaveVehiclePicturesToFile
(**)
{$APPTYPE CONSOLE}

{$R *.RES}

{$DEFINE Production}

uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  AbArcTyp,
  IdSMTP,
  IdAttachmentFile,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData,
  LoggerUnit ;

Var
  FileName: String;
  s: String;
  SList, Headers: TStringList;
  F: TextFile;
  Logger: TProgressLogger;
  InventoryTableName: String;

procedure Setup;
begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'CobaltGrandForksPhotos';
  DM.DataModuleInit;
  SList := TStringList.Create;
  SList.Delimiter := Char(9);
  Headers := TStringList.Create;
  Headers.Delimiter := Char(9);
  DM.AdsConnection1.Connect;
  //Get the current inventory data
  InventoryTableName := iuGetInventoryData(DM.InventoryQuery, DM.Market);  //was OpenInventory;
  //Delete pictures from current directory
  iuDeleteFilesFromDirectory(DM.Directory, '*.jpg', Logger);
  //GetWarrantyInfo was moved to here. It generates the DM.DifferentiatorQuery info that holds differentiator pictures and warranty descriptions
  iuGetDifferentiatorPictures(Dm.DifferentiatorQuery, DM.Market); //replaces GetWarrantyData
  Headers.Add('Dealer_id');
  Headers.Add('VIN');
  Headers.Add('Photo_url');
end;  // Proc Setup


// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  logger.AddMsg('Cleaning up objects...');
  DM.Free;
  SList.Free;
  logger.AddMsg('Finished program.');
end;


var
  VINFld: TField;
  MakeFld: TField;
  OwningLocationIDFld: TField;
  ReconPackageFld: TField;
  VehicleInventoryItemIDFld: TField;
  DiffRecNo: Integer;
  PicURLString: String;

// Unit begin
begin
  Logger := TProgressLogger.create('CobaltGrandForks');
try
//  DeleteCurrentPicsFromDirectory;
  Setup;
  FileName := 'cobalt.txt';  //  GF specific code
  AssignFile(F, FileName);
  if FileExists(FileName) then
    DeleteFile(FileName);
  ReWrite(F);

  WriteLn(F, Headers.DelimitedText);

  with DM.InventoryQuery do
  begin
    //Initialize the TField references
    VINFld := FieldByName('VIN');
    MakeFld := FieldByName('Make');
    OwningLocationIDFld := FieldByName('OwningLocationID');
    VehicleInventoryItemIDFld := FieldByName('VehicleInventoryItemId');
    ReconPackageFld := FieldByName('ReconPackage');

    First;
    while not Eof do
    begin

      //Get the list of pictures for this vehicle
      iuGetPicturesForVehicleInventoryItemID(DM.PicsQuery, InventoryTableName, VehicleInventoryItemIDFld.AsString);
      //Position the cursor on the correct Differentiator record for the location/package/make
      DiffRecNo := iuGetDiffRecordNo(DM.DifferentiatorQuery, OwningLocationIDFld.AsString, ReconPackageFld.AsString, MakeFld.AsString);
      //Save pictures for this current vehicle
      iuSaveVehiclePicturesToFile(DM.PicsQuery, DM.InventoryQuery, DM.DifferentiatorQuery, Dm.Directory, 'Picture', 4, Logger);
       //Get additional data
      PicURLString := iuGetPicURLs(DM.PicsQuery, DM.DifferentiatorQuery, 'http://67.135.158.39/files/ImagesCobalt/', char(32), 4);
      SList.Clear;
      SList.Delimiter := Char(9);
      //TODO : This must be a dealer code of some type
      SList.Add('942564');
      SList.Add(Fields.FieldByName('VIN').AsString);
      SList.Add(PicURLString);
      S := SList.DelimitedText;
      WriteLn(F, S);
      Next;
    end;  // while not eof
  end; // with
  CloseFile(F);
  logger.AddMsg('Finished creating inventory file.');

(*)
{$IFDEF Production}
  iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, '6kvf91v4', FileName, FileName, Logger);
{$ENDIF}
  iuLogFileContents(FileName, Logger);
  iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
    'Rydell GM Autocenter Pics',
    'The Rydell GM Autocenter photos file has been sent to the Cobalt ftp site.',
     DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
(**)
  Shutdown;
  except
    on e:eAdsDataBaseError do
    begin
      Logger.AddError(e.ACEErrorCode, e.Message);
      Logger.StoreLog;
    end;
    on e:Exception do
    begin
      Logger.AddError(1000, e.Message);
      Logger.StoreLog;
    end;
  end;
//  Logger.Free;
end.
