program DealerSocket;



{This program relies on two external files and an external Advantage table

  appname.ini (where appname is the name of this project)
    This file contains the valid markets, locatoin, the connection string, notification email addresses,
    ftp address and user name and directories where data will be written.

  logger.ini
    This file contains the connection information for a free Advantage table that will hold the log for this run

  External Advantage table
    This file contains the log for the run. Connection information is stored in logger.ini

  The use of this applications requires that it has access to advantage either through ADS or ALS


  VERY IMPORTANT!: The define named Production must be enabled in order for the FTP to take place
                   This define is normally commented out during testing.

}

(*

04/23/2011: what needs to be modified to generate a new feed:
2/10/12
build to local machine c:\programs7.1
  1. Pictures Directory: a physical directory to store pictures
      E:\Websites\Rydell GM Auto Center\Files\ImagesDealerCom
  2. ini file  in C:\Programs7.1
  3. DM.AppName
  4. SList.Delimiter  -- looks like Setup Slist.Delimiter is not used
  5. Logger := TProgressLogger.Create('DealerCom');
  6. FileName := '.\rydell-used.csv';
  7. FTP password: iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, 'download', FileName, FileName, Logger);
  8. Add headers if req'd
  9. Output Directory in Project->Options->Delphi Compiler

1/20/12
  use pictures downloaded by ImagesVinDash
2/9/12
Jon,

The dealer id is 2362. I changed the file name and sent it over.

See response below.

Tony

Connected by DROID on Verizon Wireless


-----Original message-----
From: Tyler Owens <towens@dealersocket.com>
To: Tony Arrington <tarrington@rydellcars.com>
Sent: Thu, Feb 9, 2012 03:02:19 GMT+00:00
Subject: RE: vehcile image import
Here�s a sample file that worked perfectly for another dealership� I think the only difference is it isn�t separated by comma�s on the image url�s, It�s separated by cell.
-- so, i'm thinking take out all the fancy delimiting, just do comma delimited
went with commatext, images still in on column
removed " and now, comments with columns are parsed into separate columns
shit, there are only 2 vehicles with comments, populate the field with an empty space
(**)

{$APPTYPE CONSOLE}
{$R *.RES}

{$DEFINE Production}

uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  IdAttachmentFile,
  IdSMTP,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData;

Var
  FileName: String;
  S: String;
  H: String;
  SList: TStringList;
  Headers: TStringList;
  F: TextFile;
  Logger: TProgressLogger;
  InventoryTableName: String;

procedure Setup;
begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'dealersocket';
  DM.DataModuleInit;
  Logger.AddMsg('Created Data Module');
  SList := TStringList.Create;

  Logger.AddMsg('Created Objects');
  DM.AdsConnection1.Connect;
  Logger.AddMsg('Connected to Database');
  InventoryTableName := iuGetInventoryData(DM.InventoryQuery, DM.Market);  //was OpenInventory;
  iuGetDifferentiatorPictures(Dm.DifferentiatorQuery, DM.Market); //replaces GetWarrantyData
//  Headers := TStringList.Create;
//  Headers.Delimiter := Char(44);
//  Headers.QuoteChar := Char(124);
//  Headers.Add('"' + 'VIN' + '"');
//  Headers.Add('"' + 'Stocknumber' + '"');
//  Headers.Add('"' + 'Comments' + '"');
//  Headers.Add('"' + 'Price' + '"');
//  Headers.Add('"' + 'Image URL' + '"');

  Headers := TStringList.Create;
  Headers.Add('VIN');
  Headers.Add('Stocknumber');
  Headers.Add('Comments');
  Headers.Add('Price');
  Headers.Add('Image URL');

end;  // Proc Setup

// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  DM.Free;
  SList.Free;
  DM.AdsConnection1.Disconnect;
  Logger.AddMsg('Finished program.');
end;

var
  StockNumberFld: TField;
  YearFld: TField;
  MakeFld: TField;
  ModelFld: TField;
  TrimLevelFld: TField;
  VINFld: TField;
  MileageFld: TField;
  PriceFld: TField;
  ColorFld: TField;
  InteriorColorFld: TField;
  EngineFld: TField;
  TransmissionFld: TField;
  OwningLocationIDFld: TField;
  ReconPackageFld: TField;
  InternetCommentFld: TField;
  VehicleInventoryItemIDFld: TField;
  VehicleItemIdFld: TField;
  BodyStyleFld: TField;
  VehicleTypeFld: TField;
  DiffRecNo: Integer;
  PicURLString: String;
  WarrantyString: String;
  OptionsString: String;

// Unit begin
begin
  Logger := TProgressLogger.Create('dealersocket');
  try
    Logger.AddMsg('Running Setup');
    Setup;
    FileName := '2362_' + FormatDateTime('mmddyyyy_hh_nn_ss', Now()) + '.csv';
    AssignFile(F, FileName);
    if FileExists(FileName) then
      DeleteFile(FileName);
    ReWrite(F);
//    H := Headers.DelimitedText;
//    H := StringReplace(H, '"|', '"', [rfReplaceAll]);
//    H := StringReplace(H, '|"', '"', [rfReplaceAll]);
    H := Headers.CommaText;
    H := StringReplace(H, '"', '', [rfReplaceAll]);
    WriteLn(F, H);
    with DM.InventoryQuery do
    begin
      //Initialize the TField references
      StockNumberFld := FieldByName('StockNumber');
      YearFld := FieldByName('Year');
      MakeFld := FieldByName('Make');
      ModelFld := FieldByName('Model');
      TrimLevelFld := FieldByName('TrimLevel');
      VINFld := FieldByName('VIN');
      MileageFld := FieldByName('Mileage');
      PriceFld := FieldByName('Price');
      ColorFld := FieldByName('Color');
      InteriorColorFld := FieldByName('InteriorColor');
      EngineFld := FieldByName('Engine');
      TransmissionFld := FieldByName('Transmission');
      OwningLocationIDFld := FieldByName('OwningLocationID');
      ReconPackageFld := FieldByName('ReconPackage');
      InternetCommentFld := FieldByName('InternetComment');
      VehicleInventoryItemIDFld := FieldByName('VehicleInventoryItemId');
      VehicleItemIdFld := FieldByName('VehicleItemId');
      BodyStyleFld := FieldByName('BodyStyle');
      VehicleTypeFld := FieldByName('VehicleType');
      First;
      while not Eof do
      begin
        //Get the list of pictures for this vehicle
        iuGetPicturesForVehicleInventoryItemID(DM.PicsQuery, InventoryTableName, VehicleInventoryItemIDFld.AsString);
        //Position the cursor on the correct Differentiator record for the location/package/make
        DiffRecNo := iuGetDiffRecordNo(DM.DifferentiatorQuery, OwningLocationIDFld.AsString, ReconPackageFld.AsString, MakeFld.AsString);
// use pictures from ImagesVinDash
         //Get additional data
        PicURLString := iuGetPicURLs(DM.PicsQuery, DM.DifferentiatorQuery, 'http://67.135.158.39/files/ImagesVinDash/', Char(44), 0);
//        SList.Clear;
//        SList.Delimiter := Char(44);
//        SList.QuoteChar := Char(124);
//        SList.Add('"' + VINFld.AsString + '"');
//        SList.Add('"' + StockNumberFld.AsString + '"');
//        SList.Add('"' + InternetCommentFld.AsString + '"');
//        SList.Add('"' + PriceFld.AsString + '"');
//        SList.Add('"' + PicURLString + '"');
//        S := SList.DelimitedText;
//        S := StringReplace(S, '"|', '"', [rfReplaceAll]);
//        S := StringReplace(S, '|"', '"', [rfReplaceAll]);
//        S := StringReplace(S, '|,', '",', [rfReplaceAll]);
        SList.Clear;
        SList.Add(VINFld.AsString);
        SList.Add(StockNumberFld.AsString);
//        SList.Add(InternetCommentFld.AsString);
        SList.Add('');
        SList.Add(PriceFld.AsString);
        SList.Add(PicURLString);
        S := SList.CommaText;
        S := StringReplace(S, '"', '', [rfReplaceAll]);
        WriteLn(F, S);
        Next;
      end;  // while not eof
    end; // with
    CloseFile(F);
    Logger.AddMsg('Finished creating inventory file.');
    iuLogFileContents(FileName, Logger);
{$IFDEF Production}
    iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, 'iipw*', FileName, FileName, Logger);
// cartiva ftp
//    iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, 'download', FileName, FileName, Logger);
    iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
      'GF DealerSocket feed',
      'The Grand Forks inventory file has been sent to the DealerSocket ftp site.',
      DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
{$ENDIF}


    Shutdown;
  except
    on e:eAdsDataBaseError do
    begin
      Logger.AddError(e.ACEErrorCode, e.Message);
      Logger.StoreLog;
    end;
    on e:Exception do
    begin
      Logger.AddError(1000, e.Message);
      Logger.StoreLog;
    end;
  end;
//  Logger.Free;
end.


