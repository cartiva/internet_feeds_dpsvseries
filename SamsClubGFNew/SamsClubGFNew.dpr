program SamsClubGFNew;
(*)
   copied HomeNetNewCarGF as a basis for this one
     no headers, no delimiters
(**)
{$APPTYPE CONSOLE}
{$R *.RES}

//  ***************************************** //
{$DEFINE Production}
//  ***************************************** //

uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  IdSMTP,
  IdAttachmentFile,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData,
  ADODB;

Var
  FileName: String;
  S: String;
  SList: TStringList;
  F: TextFile;
  Logger: TProgressLogger;
  Headers: TStringList;

procedure Setup;
begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'SamsClubGFNew';
  DM.DataModuleInit;
  SList := TStringList.Create;
  DM.AdsConnection1.Connect;
  Logger.AddMsg('Running Setup');
end;  // Proc Setup


// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  logger.AddMsg('Cleaning up objects...');
  DM.Free;
  SList.Free;
  Headers.Free;
  logger.AddMsg('Finished program.');
end;

var
  StockNumberFld: TField;
  YearFld: TField;
  MakeFld: TField;
  ModelFld: TField;
  BodyStyleFld: TField;
  VINFld: TField;
  MileageFld: TField;
  MSRPFld: TField;
  PriceFld: TField;
  ColorFld: TField;
  InteriorColorFld: TField;
  ModelNumberFld: TField;
  ADOCon: TADOConnection;
  ADOQuery: TADOQuery;
// Unit begin
begin
  Logger := TProgressLogger.create('SamsClubGFNew');
try
//  DeleteCurrentPicsFromDirectory;
  Setup;
  FileName := 'ftp4596new.csv';  //  GF specific code
//  FileName := StringReplace(FileName, '/', '', [rfReplaceAll]);
  AssignFile(F, FileName);
  if FileExists(FileName) then
    DeleteFile(FileName);
  ReWrite(F);
  try
    ADOCon := TADOConnection.Create(nil);
    ADOQuery := TADOQuery.Create(nil);
    ADOCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
    ADOQuery.SQL.Add(
      'SELECT trim(imvin) as VIN, trim(IMSTK#) as STOCK, IMYear as YEAR, IMMake as MAKE, ' +
      'IMMODL as MODEL, immcode as MODELNUMBER, imbody as BODY, ' +
      'imodom as MILES, ' +
      'imcolr as EXTCOLOR, imtrim as INTCOLOR, IMPRIC as MSRP, ' +
      'coalesce(( ' +
      'select ionval ' +
      'from inpoptf f ' +
      'inner join inpoptd d on f.ioco# = d.idco#  ' +
      'and f.iovin = i.imvin and f.ioseq# = d.idseq#  ' +
      'and iddesc = ''Internet Price''), 0) as SELLINGPRICE ' +
      'FROM INPMAST i ' +
      'inner join (  ' +
      'select gtctl#, sum(gttamt)as net ' +
      'from rydedata.glptrns ' +
      ' where trim(gtacct) in (' +
      '''123100'', ''123101'', ''123105'', ''123700'', ''123701'', ''123705'', ' +
      '''123706'', ''223000'', ''223100'', ''223105'', ''223700'', ''323102'', ''323702'', ''323703'') ' +
      'and trim(gtpost) <> ''V''  ' +
      'group by gtctl#  ' +
      'having sum(coalesce(gttamt,0)) > 1) ia on trim(i.imstk#) = trim(ia.gtctl#) ' +
      'WHERE IMSTAT = ''I'' and IMType = ''N''');
    ADOCon.Connected := True;
    ADOQuery.Connection := ADOCon;
    ADOQuery.Open;

    with ADOQuery do
    begin
      //Initialize the TField references
      StockNumberFld := FieldByName('STOCK');
      YearFld := FieldByName('YEAR');
      MakeFld := FieldByName('MAKE');
      ModelFld := FieldByName('MODEL');
      BodyStyleFld := FieldbyName('BODY');
      VINFld := FieldByName('VIN');
      MileageFld := FieldByName('MILES');
      PriceFld := FieldByName('SELLINGPRICE');
      ColorFld := FieldByName('EXTCOLOR');
      InteriorColorFld := FieldByName('INTCOLOR');
      MSRPFld := FieldByName('MSRP');
      ModelNumberFld := FieldByName('MODELNUMBER');
      First;
      while not Eof do
      begin
        SList.Clear;
        SList.Add(StockNumberFld.AsString);
        SList.Add(VINFld.AsString);
        SList.Add(InteriorColorFld.AsString);
        SList.Add(ColorFld.AsString);
        SList.Add(MileageFld.AsString);
        SList.Add(MSRPFld.AsString);  //MSRP
        S := SList.CommaText;
        WriteLn(F, S);
        Next;
      end;  // while not eof
    end; // with
  finally
    ADOCon.Free;
    ADOQuery.Free;
  end;
  CloseFile(F);
  logger.AddMsg('Finished creating inventory file.');
(**)
{$IFDEF Production}
  iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, 'cQwBTN6', FileName, FileName, Logger);
{$ENDIF}
  iuLogFileContents(FileName, Logger);
  iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
    'Sams Club New Grand Forks',
    'The Rydell GM Autocenter new inventory file has been sent to the Sams Club ftp site.',
     DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
(**)
  Shutdown;
  except
    on e:eAdsDataBaseError do
    begin
      Logger.AddError(e.ACEErrorCode, e.Message);
      Logger.StoreLog;
    end;
    on e:Exception do
    begin
      Logger.AddError(1000, e.Message);
      Logger.StoreLog;
    end;
  end;
end.
