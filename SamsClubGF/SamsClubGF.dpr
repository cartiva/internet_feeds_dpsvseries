{This program relies on two external files and an external Advantage table

  appname.ini (where appname is the name of this project)
    This file contains the valid markets, locatoin, the connection string, notification email addresses,
    ftp address and user name and directories where data will be written.

  logger.ini
    This file contains the connection information for a free Advantage table that will hold the log for this run

  External Advantage table
    This file contains the log for the run. Connection information is stored in logger.ini

  The use of this applications requires that it has access to advantage either through ADS or ALS


  VERY IMPORTANT!: The define named Production must be enabled in order for the FTP to take place
                   This define is normally commented out during testing.

}
program SamsClubGF;
(*
v1.0.0.0 deployed 7/25/2007
v1.0.0.12
  2008-12-24
  added Recon Buffer, Recon WIP, Pricing Buffer
  fixed DecodeWarranty
v1.0.0.15
  2008-01-11
  add Cadillac Certified
v3.0.0.0
  2010-01-14
  Major refactoring of project. No added functionality
  Replaced the CartivaDpsBatchDM module with DpsVSeriesBatchDM
    DpsVSeriesBatchDM raises exceptions if number of parameters is incorrect (currently only accepts 1, which is an identifier for the market)
      raises an exception is the ConnectPath key is missing or empty in the configuration file
      Cleaned up significantly, removing unused declarations and code
      Raises an exception if the wrong number of parameters is passed
      Raises an exception if the parameter does not map to a valid market
  Configuration Files:
    Added a Database key in the Global section. This key contains the ConnectPath data for the AdsConnection
  Project Source File
    Includes DpsVSeriesBatchDM instead of CartivaDpsBatchDM
    Radical change to overall structure.
    Now includes three major code segments and two var declarations
    The first Var declaration includes variables available to all code segments.
    The second Var declaration includes variables used in the main body of the project file
    The First code segment is a procedure named Setup
    The Second code segment is a procedure named Shutdown
    The Third code segment is the main body of the project
  Setup
    Creates the TDpsVSeriesBatchDM instance which contains the AdsConnection and queries that will be used
    Creates SList, which holds the string contents that are written to the file (and which is emailed)
    The creation of SList may include the definition of its delimiter, which is different for different Internet feeds
    Connects the database
    Creates the inventory table (all current vehicles that might have pictures), through a call to InventoryTableName
    Removes any previously created picture files from the designated directory through a call to iuDeleteFilesFromDirectory
    Gets the list of Differentiator pictures and warranty descriptions for the market through a call to iuGetDifferentiatorPictures
  Shutdown
    Frees the DpsVSeriesBatchDM module (closes the database connection)
    Frees SList
  Main Body
    This routine is the one that contains unique values for each Internet feed
    Calls Setup
    Creates a file reference (where the file contents of SList will be written, removing the old version first (if it exists)
    Initializes TField variables for each of the fields that need to be written (there are additional TFields created here which may not be used, but could be used in the future)
    Iterates over all VehicleInventoryItems in the InventoryTable
    Gets the list of pictures for this VehicleInventoryItem through a call to iuGetPicturesForVehicleInventoryItemID
    Moves the cursor of the DifferentiatorQuery to the correct record based on LocationID, ReconPackage, and Make (Factory only) through a call to iuGetDiffRecordNo
    Save pictures (if there is a recon package and pictures) for this vehicle to the defined directory through a call to iuSaveVehiclePicturesToFile
    Creates a string of URLS for the pictures written through the call to iuGetPicturesForVehicleInventoryItemID
    Gets the string that describes the vehicles warranty information
    Gets the string that expands the vehicles options abbreviations
    Writes this data for the current VehicleInventoryItem to a file
    FTPs and Emails this file


10/2/2010:  added parameter delimiter, char(32), to iuGetPicURLs
(**)
{$APPTYPE CONSOLE}
{$R *.RES}

// ********************************************************* //
{$DEFINE Production}
// ********************************************************* //
uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  AbArcTyp,
  IdSMTP,
  IdAttachmentFile,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData;

Var
  FileName: String;
  S: String;
  SList: TStringList;
  F: TextFile;
  Logger: TProgressLogger;
  InventoryTableName: String;

procedure Setup;
  begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'SamsClubGF';
  DM.DataModuleInit;
  SList := TStringList.Create;
  DM.AdsConnection1.Connect;
  Logger.AddMsg('Running Setup');
  //Get the current inventory data
  InventoryTableName := iuGetInventoryData(DM.InventoryQuery, DM.Market);  //was OpenInventory;
  //Delete pictures from current directory
//  iuDeleteFilesFromDirectory(DM.Directory, '*.jpg', Logger);
  //GetWarrantyInfo was moved to here. It generates the DM.DifferentiatorQuery info that holds differentiator pictures and warranty descriptions
  iuGetDifferentiatorPictures(Dm.DifferentiatorQuery, DM.Market); //replaces GetWarrantyData
end;  // Proc Setup

// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  logger.AddMsg('Cleaning up objects...');
  DM.Free;
  SList.Free;
  logger.AddMsg('Finished program.');
end;

var
  StockNumberFld: TField;
  YearFld: TField;
  MakeFld: TField;
  ModelFld: TField;
  TrimLevelFld: TField;
  VINFld: TField;
  MileageFld: TField;
  PriceFld: TField;
  ColorFld: TField;
  InteriorColorFld: TField;
  EngineFld: TField;
  TransmissionFld: TField;
  OwningLocationIDFld: TField;
  ReconPackageFld: TField;
  InternetCommentFld: TField;
  VehicleInventoryItemIDFld: TField;
  VehicleItemIdFld: TField;
  DiffRecNo: Integer;
  PicURLString: String;
  WarrantyString: String;
  OptionsString: String;

// Unit begin
begin
  Logger := TProgressLogger.create('SamsClubGF');
try
//  DeleteCurrentPicsFromDirectory;
  Setup;
  FileName := 'ftp4596.csv';  //  GF specific code
  AssignFile(F, FileName);
  if FileExists(FileName) then
    DeleteFile(FileName);
  ReWrite(F);

  with DM.InventoryQuery do
  begin

    //Initialize the TField references
    StockNumberFld := FieldByName('StockNumber');
    YearFld := FieldByName('Year');
    MakeFld := FieldByName('Make');
    ModelFld := FieldByName('Model');
    TrimLevelFld := FieldByName('TrimLevel');
    VINFld := FieldByName('VIN');
    MileageFld := FieldByName('Mileage');
    PriceFld := FieldByName('Price');
    ColorFld := FieldByName('Color');
    InteriorColorFld := FieldByName('InteriorColor');
    EngineFld := FieldByName('Engine');
    TransmissionFld := FieldByName('Transmission');
    OwningLocationIDFld := FieldByName('OwningLocationID');
    ReconPackageFld := FieldByName('ReconPackage');
    InternetCommentFld := FieldByName('InternetComment');
    VehicleInventoryItemIDFld := FieldByName('VehicleInventoryItemId');
    VehicleItemIdFld := FieldByName('VehicleItemId');

    First;
    while not Eof do
    begin
// was going to change this to VinDash picutres, but then realized, we arent even sening
// up the picurl
      //Get the list of pictures for this vehicle
//      iuGetPicturesForVehicleInventoryItemID(DM.PicsQuery, InventoryTableName, VehicleInventoryItemIDFld.AsString);
      //Position the cursor on the correct Differentiator record for the location/package/make
      DiffRecNo := iuGetDiffRecordNo(DM.DifferentiatorQuery, OwningLocationIDFld.AsString, ReconPackageFld.AsString, MakeFld.AsString);
      //Save pictures for this current vehicle
//      iuSaveVehiclePicturesToFile(DM.PicsQuery, DM.InventoryQuery, DM.DifferentiatorQuery, Dm.Directory, 'Picture', 0, Logger);

       //Get additional data
//      PicURLString := iuGetPicURLs(DM.PicsQuery, DM.DifferentiatorQuery,'http://67.135.158.39/files/ImagesSamsClub/', char(32), 0);
      WarrantyString := iuGetWarrantyString(DM.DifferentiatorQuery, DiffRecNo);
      OptionsString := iuGetOptionsString(DM.AdsQuery1,VehicleItemIdFld.AsString);
      SList.Clear;
      SList.Add(StockNumberFld.AsString);
      SList.Add(VINFld.AsString);
      SList.Add(InteriorColorFld.AsString);
      SList.Add(ColorFld.AsString);
      SList.Add(MileageFld.AsString);
      SList.Add(PriceFld.AsString);
      SList.Add(VINFld.AsString);
      SList.Add(OptionsString);
      SList.Add(WarrantyString);
      S := SList.CommaText;
      WriteLn(F, S);
      Next;
    end;  // while not eof
  end; // with
  CloseFile(F);
  logger.AddMsg('Finished creating inventory file.');
{$IFDEF Production}
  iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, 'cQwBTN6', FileName, FileName, Logger);
{$ENDIF}
  iuLogFileContents(FileName, Logger);
{$IFDEF Production}
  iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
    'Sams Club Grand Forks',
    'The Rydell GM Autocenter inventory file has been sent to the Sams Club ftp site.',
     DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
{$ENDIF}
  Shutdown;
  except
    on e:eAdsDataBaseError do
    begin
      Logger.AddError(e.ACEErrorCode, e.Message);
      Logger.StoreLog;
    end;
    on e:Exception do
    begin
      Logger.AddError(1000, e.Message);
      Logger.StoreLog;
    end;
  end;
//  Logger.Free;
end.
