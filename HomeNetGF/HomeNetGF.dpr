{This program relies on two external files and an external Advantage table

  appname.ini (where appname is the name of this project)
    This file contains the valid markets, locatoin, the connection string, notification email addresses,
    ftp address and user name and directories where data will be written.

  logger.ini
    This file contains the connection information for a free Advantage table that will hold the log for this run

  External Advantage table
    This file contains the log for the run. Connection information is stored in logger.ini

  The use of this applications requires that it has access to advantage either through ADS or ALS


  VERY IMPORTANT!: The define named Production must be enabled in order for the FTP to take place
                   This define is normally commented out during testing.

}
program HomeNetGF;
(*)
10/2/2010:  added parameter delimiter, char(32), to iuGetPicURLs
HomeNet
  FTP: iol.homenetinc.com
       hndatafeed / gx8m6
4/21/13: changed to vindash pics
2/25/17: added nice care indicator
(**)
{$APPTYPE CONSOLE}
{$R *.RES}

//  *****************************************  //
{$DEFINE Production}
//  *****************************************  //

uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  AbArcTyp,
  IdSMTP,
  IdAttachmentFile,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData;

Var
  FileName: String;
  S: String;
  SList: TStringList;
  F: TextFile;
  Logger: TProgressLogger;
  InventoryTableName: String;
  Headers: TStringList;

procedure Setup;
begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'HomeNetGF';
  DM.DataModuleInit;
  SList := TStringList.Create;
  SList.Delimiter := Char(124);
  Headers := TStringList.Create;
  Headers.Delimiter := Char(124);
  //ConnectionToDatabase
  DM.AdsConnection1.Connect;
  Logger.AddMsg('Running Setup');
  //Get the current inventory data
  InventoryTableName := iuGetInventoryData(DM.InventoryQuery, DM.Market);  //was OpenInventory;
  //Delete pictures from current directory
//  iuDeleteFilesFromDirectory(DM.Directory, '*.jpg', Logger);
  //GetWarrantyInfo was moved to here. It generates the DM.DifferentiatorQuery info that holds differentiator pictures and warranty descriptions
  iuGetDifferentiatorPictures(Dm.DifferentiatorQuery, DM.Market); //replaces GetWarrantyData
  Headers.Add('DEALERSHIPNAME');
  Headers.Add('VIN');
  Headers.Add('STOCK');
  Headers.Add('NEW-USED');
  Headers.Add('YEAR');
  Headers.Add('MAKE');
  Headers.Add('MODEL');
  Headers.Add('MODELNUMBER');
  Headers.Add('BODY');
  Headers.Add('TRANSMISSION');
  Headers.Add('TRIM');
  Headers.Add('DOORS');
  Headers.Add('MILES');
  Headers.Add('ENGINE');
  Headers.Add('DISPLACEMENT');
  Headers.Add('DRIVETRAIN');
  Headers.Add('EXTCOLOR');
  Headers.Add('INTCOLOR');
  Headers.Add('INVOICE');
  Headers.Add('MSRP');
  Headers.Add('BOOKVALUE');
  Headers.Add('SELLINGPRICE');
  Headers.Add('DATE-IN-STOCK');
  Headers.Add('CERTIFIED');
  Headers.Add('DESCRIPTION');
  Headers.Add('OPTIONS');
  Headers.Add('PhotoURLs');
  Headers.Add('CertifiedCheckListNumber');
  Headers.Add('NiceCare');
end;  // Proc Setup


// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  logger.AddMsg('Cleaning up objects...');
  DM.Free;
  SList.Free;
  Headers.Free;
  logger.AddMsg('Finished program.');
end;

var
  StockNumberFld: TField;
  YearFld: TField;
  MakeFld: TField;
  ModelFld: TField;
  TrimLevelFld: TField;
  BodyStyleFld: TField;
  VINFld: TField;
  MileageFld: TField;
  PriceFld: TField;
  ColorFld: TField;
  InteriorColorFld: TField;
  EngineFld: TField;
  TransmissionFld: TField;
  OwningLocationIDFld: TField;
  ReconPackageFld: TField;
  InternetCommentFld: TField;
  VehicleInventoryItemIDFld: TField;
  VehicleItemIdFld: TField;
  DiffRecNo: Integer;
  PicURLString: String;
  WarrantyString: String;
  OptionsString: String;
  CertifiedCheckListNumberFld: TField;

// Unit begin
begin
  Logger := TProgressLogger.create('HomeNetGF');
try
//  DeleteCurrentPicsFromDirectory;
  Setup;
  FileName := 'RydellUsed' + '.txt';  //  GF specific code
  FileName := StringReplace(FileName, '/', '', [rfReplaceAll]);
  AssignFile(F, FileName);
  if FileExists(FileName) then
    DeleteFile(FileName);
  ReWrite(F);

  WriteLn(F, Headers.DelimitedText);

  with DM.InventoryQuery do
  begin

    //Initialize the TField references
    StockNumberFld := FieldByName('StockNumber');
    YearFld := FieldByName('Year');
    MakeFld := FieldByName('Make');
    ModelFld := FieldByName('Model');
    TrimLevelFld := FieldByName('TrimLevel');
    BodyStyleFld := FieldbyName('BodyStyle');
    VINFld := FieldByName('VIN');
    MileageFld := FieldByName('Mileage');
    PriceFld := FieldByName('Price');
    ColorFld := FieldByName('Color');
    InteriorColorFld := FieldByName('InteriorColor');
    EngineFld := FieldByName('Engine');
    TransmissionFld := FieldByName('Transmission');
    OwningLocationIDFld := FieldByName('OwningLocationID');
    ReconPackageFld := FieldByName('ReconPackage');
    InternetCommentFld := FieldByName('InternetComment');
    VehicleInventoryItemIDFld := FieldByName('VehicleInventoryItemId');
    VehicleItemIdFld := FieldByName('VehicleItemId');
    CertifiedCheckListNumberFld := FieldByName('cert');

    First;
    while not Eof do
    begin

      //Get the list of pictures for this vehicle
      iuGetPicturesForVehicleInventoryItemID(DM.PicsQuery, InventoryTableName, VehicleInventoryItemIDFld.AsString);
      //Position the cursor on the correct Differentiator record for the location/package/make
      DiffRecNo := iuGetDiffRecordNo(DM.DifferentiatorQuery, OwningLocationIDFld.AsString, ReconPackageFld.AsString, MakeFld.AsString);


      //Save pictures for this current vehicle
// use vindash
//      iuSaveVehiclePicturesToFile(DM.PicsQuery, DM.InventoryQuery, DM.DifferentiatorQuery, Dm.Directory, 'Picture', 0, Logger);

       //Get additional data
      PicURLString := iuGetPicURLs(DM.PicsQuery, DM.DifferentiatorQuery, 'http://96.3.202.39/files/ImagesVinDash/', char(44), 0);
//      PicURLString := iuGetPicURLs(DM.PicsQuery, DM.DifferentiatorQuery, 'http://67.135.158.39/files/ImagesVinDash/', char(44), 0);
      WarrantyString := iuGetWarrantyString(DM.DifferentiatorQuery, DiffRecNo);
      OptionsString := iuGetOptionsString(DM.AdsQuery1,VehicleItemIdFld.AsString);

//      Write('.');
      SList.Clear;
      //TODO : Update this code. Is it similar to what is seen in other units?
      SList.Add('Rydel GM Autocenter');
      SList.Add(VINFld.AsString);
      SList.Add(StockNumberFld.AsString);
      SList.Add('Used');
      SList.Add(YearFld.AsString);
      SList.Add(MakeFld.AsString);
      SList.Add(ModelFld.AsString);
      SList.Add('');  //Model Number
      SList.Add(BodyStyleFld.AsString);
      SList.Add(TransmissionFld.AsString);
      SList.Add(TrimLevelFld.AsString);
      SList.Add('');  //Doors
      SList.Add(MileageFld.AsString);
      SList.Add(EngineFld.AsString);  //in place of cylindersCylinders
      SList.Add('');  //Displacement
      SList.Add('');  //Drivetrain
      SList.Add(ColorFld.AsString);
      SList.Add(InteriorColorFld.AsString);
      SList.Add('');  //Invoice
      SList.Add('');  //MSRP
      SList.Add('');  //BOOKVALUE
      SList.Add(PriceFld.AsString);
      SList.Add('');  //DATE-IN-STOCK
      if ReconPackageFld.AsString = 'ReconPackage_Factory' then  //certified
        SList.Add('YES')
      else
        SList.Add('NO');
      SList.Add(WarrantyString);
      SList.Add(OptionsString);
      SList.Add(PicURLString);
      SList.Add(CertifiedCheckListNumberFld.AsString);
      if ReconPackageFld.AsString = 'ReconPackage_Nice' then
        SList.Add('YES')
      else
        SList.Add('NO');
      S := SList.DelimitedText;
      WriteLn(F, S);
      Next;
    end;  // while not eof
  end; // with
  CloseFile(F);
  logger.AddMsg('Finished creating inventory file.');
(**)
{$IFDEF Production}
  iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, 'gx8m6', FileName, FileName, Logger);
{$ENDIF}
(**)
  iuLogFileContents(FileName, Logger);
  iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
    'HomeNet Used Grand Forks',
    'The Rydell GM Autocenter used inventory file has been sent to the HomeNet ftp site.',
     DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
  Shutdown;
  except
    on e:eAdsDataBaseError do
    begin
      Logger.AddError(e.ACEErrorCode, e.Message);
      Logger.StoreLog;
    end;
    on e:Exception do
    begin
      Logger.AddError(1000, e.Message);
      Logger.StoreLog;
    end;
  end;
//  Logger.Free;
end.
