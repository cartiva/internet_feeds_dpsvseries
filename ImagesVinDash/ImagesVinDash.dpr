{This program relies on two external files and an external Advantage table

  appname.ini (where appname is the name of this project)
    This file contains the valid markets, locatoin, the connection string, notification email addresses,
    ftp address and user name and directories where data will be written.

  logger.ini
    This file contains the connection information for a free Advantage table that will hold the log for this run

  External Advantage table
    This file contains the log for the run. Connection information is stored in logger.ini



}
program ImagesVinDash;
(*
2009-01-19
used MV1GF as the basis
sole purpose is to populate the ImagesVinDash with pictures for those feeds that
  the images
that way i can take out the save picture to file routines from those feeds and
  simply point the urls to ImagesVinDash
currently:
  MV1GF
  jtz
11/4/14
  added DiffRecNo := iuGetDiffRecordNo... to fix wrong differentiator being sent
(**)

{$APPTYPE CONSOLE}

{$R *.RES}

uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  AbArcTyp,
  IdSMTP,
  IdAttachmentFile,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData;

Var
  FileName: String;
  ZIPFile: String;
  ZipNoDir: string;
  S, VehicleOptions: String;
  SList: TStringList;
  F: TextFile;
  SearchRec: TSearchRec;
  Logger: TProgressLogger;
  InventoryTableName: String;
//  Headers: TStringList;

procedure Setup;
begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'ImagesVinDash';
  DM.DataModuleInit;
  SList := TStringList.Create;
  Logger.AddMsg('Running Setup');
  DM.AdsConnection1.Connect;
  Logger.AddMsg('Running Setup');
  //Get the current inventory data
  InventoryTableName := iuGetInventoryData(DM.InventoryQuery, DM.Market);  //was OpenInventory;
  //Delete pictures from current directory
  iuDeleteFilesFromDirectory(DM.Directory, '*.jpg', Logger);
  //GetWarrantyInfo was moved to here. It generates the DM.DifferentiatorQuery info that holds differentiator pictures and warranty descriptions
  iuGetDifferentiatorPictures(Dm.DifferentiatorQuery, DM.Market); //replaces GetWarrantyData
end;  // Proc Setup


// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  logger.AddMsg('Cleaning up objects...');
  DM.Free;
  SList.Free;
  logger.AddMsg('Finished program.');
end;

var
  VehicleInventoryItemIDFld: TField;
  MakeFld: TField;
  OwningLocationIDFld: TField;
  ReconPackageFld: TField;
  DiffRecNo: Integer;
  PicURLString: String;
// Unit begin
begin
  Logger := TProgressLogger.create('ImagesVinDash');
try
  Setup;
  with DM.InventoryQuery do
  begin
    VehicleInventoryItemIDFld := FieldByName('VehicleInventoryItemId');
    MakeFld := FieldByName('Make');
    OwningLocationIDFld := FieldByName('OwningLocationID');
    VehicleInventoryItemIDFld := FieldByName('VehicleInventoryItemId');
    ReconPackageFld := FieldByName('ReconPackage');
    First;
    while not Eof do
    begin
      //Get the list of pictures for this vehicle
      iuGetPicturesForVehicleInventoryItemID(DM.PicsQuery, InventoryTableName, VehicleInventoryItemIDFld.AsString);
      //Position the cursor on the correct Differentiator record for the location/package/make
      DiffRecNo := iuGetDiffRecordNo(DM.DifferentiatorQuery, OwningLocationIDFld.AsString, ReconPackageFld.AsString, MakeFld.AsString);
//      //Save pictures for this current vehicle
      iuSaveVehiclePicturesToFile(DM.PicsQuery, DM.InventoryQuery, DM.DifferentiatorQuery, Dm.Directory, 'Picture', 0, Logger);
      Next;
    end;  // while not eof
  end; // with
//  CloseFile(F);
  logger.AddMsg('Finished saving pics to ImagesVinDasy.');
  iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
      'ImagesVinDash',
      'The GF inventory & images zip file has been sent to the MV1 ftp site.',
      DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
(**)
  Shutdown;
  except
    on e:eAdsDataBaseError do
    begin
      Logger.AddError(e.ACEErrorCode, e.Message);
      Logger.StoreLog;
    end;
    on e:Exception do
    begin
      Logger.AddError(1000, e.Message);
      Logger.StoreLog;
    end;
  end;
//  Logger.Free;
end.
