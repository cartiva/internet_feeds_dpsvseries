{This program relies on two external files and an external Advantage table

  appname.ini (where appname is the name of this project)
    This file contains the valid markets, locatoin, the connection string, notification email addresses,
    ftp address and user name and directories where data will be written.

  logger.ini
    This file contains the connection information for a free Advantage table that will hold the log for this run

  External Advantage table
    This file contains the log for the run. Connection information is stored in logger.ini

  The use of this applications requires that it has access to advantage either through ADS or ALS


  VERY IMPORTANT!: The define named Production must be enabled in order for the FTP to take place
                   This define is normally commented out during testing.

1/19/17
  added COLOR_CODE attribute

}
program HomeNetNewCarGF;
(*)
   copied HomeNetGF to do this new car version
(**)
{$APPTYPE CONSOLE}
{$R *.RES}

// ***************************************************************** //
{$DEFINE Production}
// ***************************************************************** //

uses
  SysUtils,
  StrUtils,
  classes,
  IdMessage,
  DB,
  IDFTPCommon,
  IdSMTP,
  IdAttachmentFile,
  InvUtils,
  ProgressLoggerU,
  DpsVSeriesBatchDMU,
  AdsData,
  ADODB;

Var
  FileName: String;
  S: String;
  SList: TStringList;
  F: TextFile;
  Logger: TProgressLogger;
  Headers: TStringList;

procedure Setup;
begin
  DM := TDpsVSeriesBatchDM.Create(nil);
  DM.AppName := 'HomeNetNewCarGF';
  DM.DataModuleInit;
  SList := TStringList.Create;
  SList.Delimiter := Char(124);
  Headers := TStringList.Create;
  Headers.Delimiter := Char(124);
  //ConnectionToDatabase
  DM.AdsConnection1.Connect;
  Logger.AddMsg('Running Setup');

  Headers.Add('DEALERSHIPNAME');
  Headers.Add('VIN');
  Headers.Add('STOCK');
  Headers.Add('NEW-USED');
  Headers.Add('YEAR');
  Headers.Add('MAKE');
  Headers.Add('MODEL');
  Headers.Add('MODELNUMBER');
  Headers.Add('BODY');
  Headers.Add('TRANSMISSION');
  Headers.Add('TRIM');
  Headers.Add('DOORS');
  Headers.Add('MILES');
  Headers.Add('CYLINDERS');
  Headers.Add('DISPLACEMENT');
  Headers.Add('DRIVETRAIN');
  Headers.Add('EXTCOLOR');
  Headers.Add('INTCOLOR');
  Headers.Add('INVOICE');
  Headers.Add('MSRP');
  Headers.Add('BOOKVALUE');
  Headers.Add('SELLINGPRICE');
  Headers.Add('DATE-IN-STOCK');
  Headers.Add('CERTIFIED');
  Headers.Add('DESCRIPTION');
  Headers.Add('OPTIONS');
  Headers.Add('COLOR-CODE');
end;  // Proc Setup


// Closes all Connections and Frees any Streams, StringLists, etc.
procedure Shutdown;
begin
  logger.AddMsg('Cleaning up objects...');
  DM.Free;
  SList.Free;
  Headers.Free;
  logger.AddMsg('Finished program.');
end;

var
  StockNumberFld: TField;
  YearFld: TField;
  MakeFld: TField;
  ModelFld: TField;
  BodyStyleFld: TField;
  VINFld: TField;
  MileageFld: TField;
  MSRPFld: TField;
  PriceFld: TField;
  ColorFld: TField;
  InteriorColorFld: TField;
  ModelNumberFld: TField;
  InvoiceFld: TField;
  ColorCodeFld: TField;
  ADOCon: TADOConnection;
  ADOQuery: TADOQuery;
// Unit begin
begin
  Logger := TProgressLogger.create('HomeNetNewCarGF');
try
//  DeleteCurrentPicsFromDirectory;
  Setup;
  FileName := 'RydellNew' + '.txt';  //  GF specific code
  FileName := StringReplace(FileName, '/', '', [rfReplaceAll]);
  AssignFile(F, FileName);
  if FileExists(FileName) then
    DeleteFile(FileName);
  ReWrite(F);
  try
    ADOCon := TADOConnection.Create(nil);
    ADOQuery := TADOQuery.Create(nil);
    ADOCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
    ADOQuery.SQL.Add(
      'SELECT trim(imvin) as VIN, trim(IMSTK#) as STOCK, IMYear as YEAR, IMMake as MAKE, ' +
      'IMMODL as MODEL, immcode as MODELNUMBER, imbody as BODY, ' +
      'imodom as MILES, ' +
      'imcolr as EXTCOLOR, imtrim as INTCOLOR, IMPRIC as MSRP, ' +
      'color_code as COLORCODE, ' +
      'coalesce(( ' +
      'select ionval ' +
      'from rydedata.inpoptf f ' +
      'inner join rydedata.inpoptd d on f.ioco# = d.idco#  ' +
      'and f.iovin = i.imvin and f.ioseq# = d.idseq#  ' +
      'and iddesc = ''Internet Price''), 0) as SELLINGPRICE, ' +
      'coalesce(( ' +
      'select ionval ' +
      'from rydedata.inpoptf f ' +
      'inner join rydedata.inpoptd d on f.ioco# = d.idco#  ' +
      'and f.iovin = i.imvin and f.ioseq# = d.idseq#  ' +
      'and iddesc = ''Flooring/Payoff''), 0) as INVOICE ' +
      'FROM rydedata.INPMAST i ' +
      'inner join (  ' +
      'select gtctl#, sum(gttamt)as net ' +
      'from rydedata.glptrns ' +
      ' where trim(gtacct) in (' +
      '''123100'', ''123101'', ''123105'', ''123700'', ''123701'', ''123705'', ' +
      '''123706'', ''123707'', ''223000'', ''223100'', ''223105'', ''223700'', ''323102'', ''323702'', ''323703'') ' +
      'and trim(gtpost) <> ''V''  ' +
      'group by gtctl#  ' +
      'having sum(coalesce(gttamt,0)) > 1) ia on trim(i.imstk#) = trim(ia.gtctl#) ' +
      'WHERE IMSTAT = ''I'' and IMType = ''N''');
    ADOCon.Connected := True;
    WriteLn(F, Headers.DelimitedText);
    ADOQuery.Connection := ADOCon;
    ADOQuery.Open;

    with ADOQuery do
    begin
      //Initialize the TField references
      StockNumberFld := FieldByName('STOCK');
      YearFld := FieldByName('YEAR');
      MakeFld := FieldByName('MAKE');
      ModelFld := FieldByName('MODEL');
      BodyStyleFld := FieldbyName('BODY');
      VINFld := FieldByName('VIN');
      MileageFld := FieldByName('MILES');
      PriceFld := FieldByName('SELLINGPRICE');
      ColorFld := FieldByName('EXTCOLOR');
      InteriorColorFld := FieldByName('INTCOLOR');
      MSRPFld := FieldByName('MSRP');
      ModelNumberFld := FieldByName('MODELNUMBER');
      InvoiceFld := FieldByName('INVOICE');
      ColorCodeFld := FieldByName('COLORCODE');
      First;
      while not Eof do
      begin
        SList.Clear;
        SList.Add('Rydel GM Autocenter');
        SList.Add(VINFld.AsString);
        SList.Add(StockNumberFld.AsString);
        SList.Add('NEW');
        SList.Add(YearFld.AsString);
        SList.Add(MakeFld.AsString);
        SList.Add(ModelFld.AsString);
        SList.Add(ModelNumberFld.AsString);  //Model Number
        SList.Add(BodyStyleFld.AsString);
        SList.Add(''); //Transmission
        SList.Add(''); //Trim
        SList.Add('');  //Doors
        SList.Add(MileageFld.AsString);
        SList.Add(''); //Cylinders
        SList.Add('');  //Displacement
        SList.Add('');  //Drivetrain
        SList.Add(ColorFld.AsString);
        SList.Add(InteriorColorFld.AsString);
        SList.Add(InvoiceFld.AsString);  //Invoice
        SList.Add(MSRPFld.AsString);  //MSRP
        SList.Add('');  //BOOKVALUE
        SList.Add(PriceFld.AsString);
        SList.Add('');  //DATE-IN-STOCK
        SList.Add('');  //Certified
        SList.Add('');  //Descriptions
        SList.Add('');  //Options
        SList.Add(ColorCodeFld.AsString);  //COLOR-CODE
        S := SList.DelimitedText;
        WriteLn(F, S);
        Next;
      end;  // while not eof
    end; // with
  finally
    ADOCon.Free;
    ADOQuery.Free;
  end;
  CloseFile(F);
  logger.AddMsg('Finished creating inventory file.');
(**)
{$IFDEF Production}
  iuFtpFile(DM.FTP, DM.FTPaddress, Dm.User, 'gx8m6', FileName, FileName, Logger);
{$ENDIF}
(**)
  iuLogFileContents(FileName, Logger);
{$IFDEF Production}
  iuSendEmail(DM.SMTP, FileName, 'CartivaDPS@cartiva.com', DM.Notifications.CommaText,
    'HomeNet New Grand Forks',
    'The Rydell GM Autocenter new inventory file has been sent to the HomeNet ftp site.',
     DM.Message, DM.Attachment, 'mail.cartiva.com', 'cartivadps', 'password', Logger);
{$ENDIF}
  Shutdown;
  except
    on e:eAdsDataBaseError do
    begin
      Logger.AddError(e.ACEErrorCode, e.Message);
      Logger.StoreLog;
    end;
    on e:Exception do
    begin
      Logger.AddError(1000, e.Message);
      Logger.StoreLog;
    end;
  end;
//  Logger.Free;
end.
